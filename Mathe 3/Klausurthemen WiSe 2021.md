# Klausurthemen Mathe 3

Klausurthemen für das WiSe 2020/21, wie sie von Prof. Struckmeier in der Vorlesung angekündigt wurden.

## Analysis III

Oberflächenintegrale entfallen.

Klausur besthet aus 4 Aufgaben zu folgenden Themen:

1. Taylor-Polynom bis bestimmten Grad berechnen
    - Funktion und Entwicklungspunkt gegeben
    - Keine Fehlerabschätzung dieses Jahr
2. Gradient und Hessematrix einer Funktion bestimmen
    - Stationäre Punkte bestimmen und klassifizieren
      - Minima, Maxima, Sattelpunkte
3. Extremwertbestimmung unter Nebenbedingung
   - Lagrange Multiplikaorenregel verwenden
   - Wichtig: Regularitätsbedingung überprüfen
4. Kurvenintegrale 2. Art  
   - Vektorfelder gegeben
   - Aufgabe: Welche Vektorfelder besitzen Potential
   - Dann: Potential ausrechnen, wenn vorhanden
   - Dann: Kurvenintegrale berechnen

Laut Struckmeier: Kleine Änderungen vorbehalten; Mehr kommt auf jeden Fall nicht dazu

### Themen, die ich wichtig finde / Lernplan aus Klausurthemen

- [x] Taylor Polynome berechnen bei geg. Funktion um Entwicklungspunkt
- [x] Gradient und Hessematrix bestimmen
  - [ ] Stationäre Punkte bestimmen
  - [ ] Minima, Maxima, Sattelpunkte bestimmen
  - [ ] Stationäre Punkte Klassifizieren
- [ ] Extremwertbestimmung unter Nebenbedingungen
  - [ ] Lagrange'sche Multiplikatorenregel
  - [ ] Regularitätsbedingung
- [ ] Kurvenintegrale 2. Art bestimmen bei geg. Vektorfeld

## Differentialgleichungen I

Kapitel 4 und 5 nicht Klausurrelevant. Ljapunov auch nicht.

Klausur besteht aus 4 Aufgaben

1. Zwei teilaufgaben zu Elementare Lösungsmethoden (Abschnitt 1.2)
   1. Verschiedene DGL gegeben. Typ A-F muss zugeordnet werden; aber keine Lösung
   2. Typ-D DGL (Bernulli S. 27) gegeben. DGL muss nicht gelöst werden
2. Exakte DGL
    - Entscheiden, welche DGL exakt sind und begründen
    - Für eine Exakte DGL Potential bestimmen; Dann Allgemeine Lösung aus Potential bestimmen
3. Kapitel 3: Lineare DGL:
   - Systeme erster Ordnung mit konstanten Koeefizienten (nur 2x2 System)
   - Reelles Fundamentalsystem ausrechnen auch wenn Eigenwerte Komplex sind
4. Stabilität (Wichtig: Stabilitätssatz II)
   - Strikt stabil, gleichmäßig stabil, instabil auseunanderhalten können
   - Dafür: Eigenwerte berechnen
   - Wird 3x3 System sein, welches von einem Parameter abhängt
   - Aufgabe: In Abhängig von Parameter entscheiden, ob die Nulllösing instabil, gleichmäßig stabil oder instabil ist
   - Tipp zu 3x3-Systemen: Anschauen, wie man charakteristisches Polynom effizient bestimmen kann über Entwicklung nach einer Zeile oder Spalte -> **Sarrus**

### Themen, die ich wichtig finde / Lernplan aus Klausurthemen

- [x] DGL Typen (A-F) Auseinander halten können
- [ ] Bernoulli auf lineare DGL zurückführen
- [ ] Bestimmen, welche DGL exakt sind
- [ ] Potential einer exakten DGL bestimmen
