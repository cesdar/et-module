# Infos aus dem Semester

## DGL 2

## Tipps zu übung P4

1a) Klassifikation über Determinante von $A$ Matrix. Wenn kleiner 0 -> hyperbolische PDE

1b) $\sigma(A) = \{1,3\}.$

1c) Wir wollen Koordinatentransformation durchführen
Dafür brauchen wir: Unitäre diagonalisierungsmatrix $s$. Besteht aus normierten Eigenvektoren -> Orthonormalbasis

## Komplexe Funktionen

- Möbius Transformation gerne Klausuraufgabe

# Klausurthemen aus Beratung

## DGL 2

### Themen laut Strucki

- Quasilinerare DGL mit Chrakteristikenmethode
- Lösen einer Erhaltungsgleichung mit u_T+fU_x = 0 als Flussfunktion
  - Hier für Flussfunktion die nicht zur Burgersgleichung gehöret Verdünnungs/Stoßwellen berechnen -> nicht U^2/2
- Produktansatz

### Hinweise Kiani

### Zu bearbeitende Themen

- einfache gewöhnliche DGL lösen
  - Seprierbare (vgl. z.B. Charakteristiken Methode)
  - Lineare mit konstanten Koeffizienten (vgl. z.B. Wärmeleitungsgleichung)
    - $y'(t)+\alpha y(t) = p_n(t) \cdot e^{\mu t}$ mit Polynom n-ten Grades $p_n$
- Fourier Koeffizienten berechnen (Blatt 5/6)
- Charakteristiken Methode
- Burgers Gleichung: zwei Verdünnungswellen, zwei Stoßwellen
  - Sprungbedingungen für $u_t+(f(u))_x=0,f(u)\neq \frac{u^2}{2}$ (H3.1b)
  - evtl. Zeichnen der Charakteristik
- Charakteristiken Eigenschaften
- Entropielösung für $u_t + (f(u))_x=0,f(u)\neq \frac{u^2}{2}$
- ...

## Komplexe Funktionen

### Themen laut Strucki

- Bilder irghendwelcher Mengen (Quadrate, Streifen, Kreise, ...) unter Kombination elementarer Funktionen
- Möbius; Auch Bestimmung der Möbius Transformation unter gegebenen z-Werten + Bestimmung von Bildern unter gegebener Möbius Transformation
- (Integrale, ...) -> Hat Strucki nicht viel zu gesagt, ist aber trotzdem wichtig