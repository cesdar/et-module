# Halbleiterschaltungstechnik Formelsammlung und Wissenswertes

## pn-Diode

- Durchlassspannung Si-Diode: ~0,7V
- Dioden dienen als "elektrisches Ventil" und leiten, wenn das Potential der Anode ungefähr um die Durchlassspannung höher ist als das an der Kathode.
  - Durchlassrichtung (Flusspolung): Strom fließt
  - Sperrrichtung (Sperrpolung): nur Sättigungsstrom fließt; nahe 0mA

#### Schockley-Gleichung $I_D = f(U_D)$

$$
    I_D = I_S \cdot \left(e^{\frac{d\cdot U_D}{n \cdot k\cdot T}}-1\right) = I_S \cdot \left(e^{\frac{U_D}{n \cdot U_{Temp}}}-1\right)
$$

$U_{Temp}$ ist Temperaturspannung der Diode und lässt sich durch $\frac{k\cdot T}{q}$ ausdrücken. Für $I_D =const$ gilt: $\frac{\text{d}U_d}{\text{d}T} = -1 ... -3 \frac{mV}{K} \approx -2 \frac{mV}{K}$.
Pro Grad erwärmung sinkt $U_D$ somit um $\approx 2mV$ pro Kelvin.

Daraus folgt für die Temperaturabhängigkeit, dass mit zunehmender Temperatur die Diodenkennlinie nach links bzw. oben verschoben wird.

Die Verlustleistung in einer Diode kann wie üblich über das Produkt aus Strom und Spannung berechnet werden: $P_V = U_D \cdot I_D$

### Arbeitspunktberechnung

Es gibt drei Lösungsansätze:
1. graphisch
2. iterativ (numerisch)
3. konstantes $U_D$

#### 1. Graphisches Verfahren

Es werden sowohl Diodenkennlinie und Widerstandskennlinie jeweils für die gegebene Schaltung in Abhängigkeit von $U_D$ in ein Diagramm gezeichnet und dann der Schnittpunkt bestimmt.

#### 2. Numerische Arbeitspunktberechnung

Verwendung des Newton Verfahrens zur Nullstellenbestimmung

#### 3. Modell der konstanten Durchlassspannung

Es wir eine konstante Spannung angenommen, die über der Diode in Flussrichtung abfällt. Diese kann für Silizium als $\approx$ 0,7 V angenommen werden.#

### Differentieller Widerstand

....
Bis Folie 38
### Faustregeln Diode

- Pro Erwärmung um ein Grad sinkt $U_{Temp}$ um $\approx 2mV$
- Etwa alle 60mV Spannung über einer idealen pn-Übergangs Diode (n=1) verzehnfacht sich der Strom

Ab Folie 65

## Transistor

![](BJT_Aufbau_NPN_PNP_Diodenmodell.JPG)

Funktionprinzip Transistor: Ein kliner Basisstrom verursacht einen um den Faktor B größeren Kollektorstrom: $I_C=B\cdot I_B$

Der Kollektor-Emitter Zweig wirkt wie eine stromgesteuerte Spannungsquelle.

Die Basis-Emitter Spannung ist $\approx 0,7 V$

Gleichstromverstärkung eines Transistors (in Emitterschaltung): $B=\frac{I_C}{I_B}$

$B$ ist näherungsweise konstant.