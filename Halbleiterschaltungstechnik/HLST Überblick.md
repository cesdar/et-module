# Halbleiterschaltungstechnik Überblick

## Themen

- Silizium Halbleitereffekte
- HL-Dioden
  - Ausbildung der RLZ
  - Betrieb in Durchlass- und Sperrrichtung
  - Diodenkennlinie $\frac{I_D}{U_D}$: 3 Arbeitsbereiche

## Fragen für Klausur

### Dioden

- Newton Verfahren relevant?

### BJT

- Funktionsprinzip auf Elektronenebene wichtig?
- Technische Realisiserung in Silizium wichtig?

## Anschauen für Klausur

### Dioden

- Berechnung Differentieller Widerstand
- Dioden Arbeitspunktbestimmung über alle 3 Möglichkeiten

## Faustformeln

### Si-Dioden

Es gibt verschiedene Typen und Durchlassspannungen. Für Si typisch ist 0,7 V.

Schockley-Gleichung: $I_D = I_s \cdot \left(e^{\frac{d\cdot U_D}{n\cdot k\cdot T}}-1\right)$

Temperaturspannung bei Raumtemperatur ca. 25 mV. Ändert sich ~2mV pro Kelvin.

## Überblick

### Si-Diode

Eine Silizium Diode besteht aus zwei unterschiedlich dotierten Halbleiterbereichen. Hierbei ist der eine n- und der andere p-dotiert. Sofoert nach Zusammenbringen der beiden Bereiche bildet sich eine Zone aus in der die überschüssigen Löcher aus der p-Schicht mit Elektronen aus der n-Schicht rekombinieren. Dies geschieht so lange bis sich durch die Ladungsträgertrennung ein gewisses Elektrisches Feld gebildet hatm, welches diesem Drang entgegenwirkt. Das so entstehende hochohmige Gebiet nennt man Sperrgebiet oder auch Raumladungszone (RLZ).

Legt man jetzt eine Spannung so an der Diode an, dass diese entgegen der RLZ wirkt wird diese abgebaut und es kann ein Strom fließen. Eine andersrum gepolte Spannung sorgt hingegen dafür, dass die RLZ weiter ausgedehnt wird wodurch, bis auf den Sättigungsstrom im der Größenordnung nA, kein Strom fließen kann.

Die Diodenkennlinie kann durch die Schokley Gleichung beschrieben werden.

### Arbeitspunktbestimmung Diode

#### Newton Verfahren

#### Differentieller Widerstand

Der Differentielle Widerstand ist die Steigung bzw. Ableitung der Diodenkennlinie und ergibt sich zu $r_D = \frac{\text{d}U_D}{\text{d}I_D}|_A$ mit $A$ als Punkt an den dem $R_D$ berechnet wird. Der Differentielle Widerstand wird dazu verwendet, um Baueile im Kleinsignalfall linearisieren zu können. Da bei Kleinsignalen die Amplitude meist nur eine wenige mV ist kann man darüber eine Näherung machen, die einem die Rechenarbeit deutlich erleichtert und trotzedm nur kleine Fehler in die Rechnung bringt. 

Bevor man den Differentiellen Widerstand aber für eine Konkrete Rechnung bestimmen kann, ist es notwendig den Arbeitspunkt zu bestimmen (s. Oben).

## Glossar

Differntieller Widerstand: $r_D = \frac{\text{d}U_D}{\text{d}I_D}|_A$

Differentieller Leitwert (einer Diode): 
$$
    g_D = \frac{1}{r_d} = \frac{\text{d}I_D}{\text{d}U_D} \left( = \frac{\text{d}}{\text{d}U_D} \left(I_S \cdot e^{\frac{U_D}{n\cdot U_{Temp}}}\right) = \frac{1}{n\cdot U_{Temp}} I_S \cdot e^{\frac{U_D}{n\cdot U_{Temp}}} \right)
$$

Daraus ergibt sich für eine Diode: 
$$r_D = \frac{n \cdot U_{Temp}}{I_D}$$