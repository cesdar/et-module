# Halbleiterschaltungstechnik Zusammenfassung

## pn-Sperrschichtdioden

Silizium ist das heutzutage mesitverbreitetste Halbleitermaterial. In seiner Reinform ist es relativ schlecht leitfähig und hat keine besonderen Eigenschaften. Wenn man aber jetzt anfängt das 4-Wertige (4 Außenelektronen) Silizumgitter mit Fremdatomen (3- oder 5-Wertig) zu verunreinigen (dotieren) wird es selbst bei geringer Anzahl an Dotieratomen relativ gut leitfähig.

Bringt man nun eine n- und p- dotierte Schicht (Elektronen bzw. Löcherüberschuss) aneinander, so gleichen sich in einem gewissen Bereich, der Raumladungszone, überschüsse Elektronen und Löcher aus. In dieser Zone steigt der Widerstand des Halbleitermaterials deutlich im Gegensatz zum rein dotierten, deutlich an. Wenn man jetzt ein Feld von Außen anlegt kann die Dicke der Raumladungstone verändert und bis aus 0 runtergebracht werden.

Dann hat man eine **pn-Diode** gebaut, welche Strom nur in einer Richtung durchlässt und in der anderen Richtung sperrt.

Bei einer **realen Diode** muss allerdings beachtet werden, dass diese grob erst ab einer gewissen Druchlassspannung leitet. Unterhalb dieser Spannung ist der Stromfluss veranachlässigbar klein und steigt dann exponentiell an in Abhängigkeit der Spannung über der Diode.

### Die Schockley-Gleichung

Das Verhalten einer realen Diode kann annähernd durch die **Schockley-Gleichung** beschrieben werden.

$$
    I_D = I_S\cdot\left(e^{\frac{q\cdot U_D}{n\cdot k\cdot T}}-1\right) = I_S\cdot\left(e^{\frac{U_D}{n\cdot U_{Temp}}}-1\right)
$$

Dabei ist $I_D$ der resultierende Strom, welcher durch die Diode fließt, wenn die Spannung $U_D$ an ihr anliegt. $I_s$ ist der Sättigungssperrstrom, welcher fließt, wenn die Diode in Sperrrichtung gepolt ist. $n$ ist der Emisisonskoeffizient und bewegt sich bei realen Si-Dioden zwischen 1 ... 2. $U_{Temp}$ lässt sich aus der Elementarladung $q$, der Boltzmann Konstanten $k$ und der Raumtemperatur $T$ in Kelvin berechnen. 

Effektiv wird die Diodenkennlinie durch Temperaturerhöhung nach links bzw. oben verschoben. Für ein konstantes $I_D$ ergibt sich die Temperaturabhängigkeit zu:

$$
    \frac{\text{d}U_D}{\text{d}T} = -1 ... -3 \frac{\text{mV}}{\text{K}} \approx -2 \frac{\text{mV}}{\text{K}}
$$

> Bei Erwärmung einer Diode sinkt $U_D$ um ca. 2mV je Kelvin

# Transistoren

## Bipolartransistoren

Intern können Bipolartransistoren durch 2 Dioden beschrieben werden. Zu beachten ist aber, dass die Funktion eines BJT nicht alleine durch zusammenschaltung zweier Dioden geschehen kann, da es in der Mitte ein zusammenhängendes, möglichst kleines gleich dotiertes Halbleitergebiet geben muss.

Bipolar Juction Transistors (BJT) gibt es in 2 Bauformen: Einmal den PNP und den NPN Transistor. Diese untershcheiden sich im Aufbau nur durch die Dotierung der Halbleitergebiete. 

BJT's haben 3 Anschlüsse: Die Basis (B) in der Mitte und Außen den Kollektor (C) sowie den Emitter. Hierbei soll noch einmal angemerkt werden, dass Kollektor und Emitter geometrisch nicht gleich sind und nicht einfach ausgetauscht werden können.

Der Standard Betrieb mit großer Verstärkung ist, dass die Basis-Emitter Diode in Durchlassrichtung betrieben wird. Sobald durch diese ein Strom fließt führt dies dazu, dass auch der Kollektor-Emitter-Pfad leitend wird und ein Strom von $I_C = B \cdot I_B$ fließen kann.

Im üblichen aktien Normalbetrieb eines NPN- (bzw. PNP-)Transistors gelten folgende Bedingungen:
- Emitter liegt auf niedrigsten (bzw. höchstem) Potential
- Kollektor liegt auf einem hohen (bzw. niedrigen) Potential
- Basis liegt so, dass die Basis-Emitter-Diode leitet und Basis-Kollektor-Diode sperrt

Der Transistor kann grudsätzlich in drei verschiedenen Betriebsmodi betrieben werden:
- Sperrbetrieb: ... **TODO**

### Gleichstromverstärkung eines BJT

## MOS Feldeffekttransistoren

Bei einem **MOSFET** (Metal-Oxide-Semiconductor Field-Effect Transistor) wird die Leitfähigkeit des Kanals durch ein angelegtes E-Feld gesteuert. Dies hat den großen Vorteil, dass dieser Leistungslos schalten kann. Mosfets können grob an Zwei Merkmalen auseinandergehalten werden:
- Dotierung: Es gibt N- und P-MOS Transistoren. Hier wird jeweils angegeben, welche Ladungsträger hauptsächlich im Kanal zur Leitung beitragen im Durchlassbereich. Dabei ist der N-MOS Kanal P-Dotiert und es sind haupsächlich Elektronen an der Leitung beteiligt. Beim P-MOS ist dies genau umgekehrt und hier sind es die Löcher, die im N-Dotierten Kanal leiten.
- Schaltverhalten: MOSFETs gibt es sowohl als **Verarmungs (Depletion) und Anreicherungs(Enhancement)** Typ. Hier wird jeweils angegeben, was das gewünschte Schaltverhalten ist. Beim **Depletion** Typ ist der Kanal ohne angelegte Gate Spannung leitend und durch angelegte Gate Spannung werden die Ladungsträger aus dem Kanal verdrängt oder verarmt. Beim **Enhancement** Typ hingegen sperrt der Kanal ohne Gate Spannung und durch Anreicherung von Ladungsträgern durch ein E-Feld an der Gate Elektrode ist Leitung möglich.

## Vergleich MOSFET Schaltungen

| Bezugselektrode | Verstärkung | Phasendrehung |        Anwendung        |
| :-------------: | :---------: | :-----------: | :---------------------: |
|     Source      |             |               |                         |
|      Drain      |    $<1$     |     $0°$      | Puffer, Impedanzwandler |
|      Gate       |   groß, wie bei Source Schaltung          |   $0°$            |  HF-Spannungsverstärker                       |


|  | Source | Drain | Gate |
| :-: | :-: | :-: | :-: |
| Spannungsverstärkung | groß | ~=1 | groß |
| Stromverstärkung | $\to \infty$ | $\to\infty$ | 1 |
Eingangswiderstand | $\to\infty$ | $\to\infty$ | klein |
Ausgagswiderstand | mittel | klein | mittel |
| Phasendrehung | $180°$ | $0°$ | $0°$ |
| Typ. Anwendung |  | Buffer | HF-Verstärker |

### Bestimmung Ein- und Ausgangswiderstand

Bei der Ein- und Ausgangsspannung

## Drain Schaltung

Bei der Drain Schaltung ist der Drain Anschluss die Gemeinsame Bezugselektrode.

Diese Schaltung kann u.a. als Pufferstufe/Bufferstage verwender werden. Die Verstärkung beträgt bis zu knapp 1. Die Ein- und Ausgangsspannung sind Phasengleich.

![Common Drain Stufe Schaltplan](mosfet_commondrain_schematic.png)

Die **Spannungsverstärkung** ergibt sich zu:

$$
    A_U = \frac{u_{Aus}}{u_{Ein}} = \frac{R_S\cdot g_m u_{GS}}{U_{GS} + R_S\cdot g_m u_{GS}} = \frac{R_S}{\frac{1}{g_m} + R_S} < 1
$$

#### Ein- und Ausgangswiderstand

- Eingangswiderstand:  $r_{Ein} = R_1||R_2$
- Ausgangswiderstand: $r_{Aus} = \frac{\text{Leerlaufwechselspannung}}{\text{Kurzschlusswechselstrom}} = \frac{u_{Aus}}{i_K}$

## Common Gate Schaltung

Grundsätlich gleicher Aufbau mit einem Transistor. Gate hat konstantes Potential; Ein- und Ausgang sind an Source und Drain.

Typische Anwendung ist als Spannungsverstärker in HF-Schaltungen, da relativ hohe Verstärkung und kein Miller-Effekt auftritt durch die parasitären Rückwirkungskapazitäten. -> Vorteil: relativ große Gatekapazität muss nicht umgeladen werden. Somit reicht kleienr Antennendraht aus

## Mehrstufige Verstärker

#### Bsp: Audioverstärker

Mikrofon liefert rel. Hochohmig 10mV mit 880 Ohm Quellenwiederstand, welcher zunächst mit 3 Soruce Schaltungen auf ca. 30x verstärkt werden. Am Ende soll ein Niederohmiger Lautsprecher mit 25 Ohm Impedanz getrieben werden, wofür noch eine Drainstufe nachgeschaltet werden kann welche als Buffer fungiert und eine Wandlung der Ausgangsimpedanz durchführt.

## MOSFET Differenzstufe

Probleme bei Sourceschlatung:
- Arbeitspunktempfindlich
- Temperaturabhängig
- AC-Kopplung am Ein- und Ausgang: nur AC Signale möglich
- Große Koppelkondensatoren verrinern die Eckfrequenz

Vorteil des Differenzverstärkers ist geringe Temperaturabhängigkeit, wenn beide Transistoren im gleichen Gehäuse.