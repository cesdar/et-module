# TET II Formelsammlung

## Allgemeines Vektoralgebra

### Laplace Operator

$$\Delta f = div(grad f) = \frac{\partial^2f}{\partial x^2} + \frac{\partial^2f}{\partial y^2} + \frac{\partial^2f}{\partial z^2}$$

| Koordinatensystem | Laplace Operator |
| --- | --- |
| kartesisch | $\Delta f = \frac{\partial^2f}{\partial x^2} + \frac{\partial^2f}{\partial y^2} + \frac{\partial^2f}{\partial z^2}$ |
| Polar | $\Delta f = \frac{1}{r} \frac{\partial f}{\partial r} + \frac{\partial^2f}{\partial r^2} + \frac{1}{r^2} \frac{\partial^2f}{\partial \phi^2}$ |
| Zylinder| $\Delta f = \frac{1}{r} \frac{\partial f}{\partial r} + \frac{\partial^2f}{\partial r^2} + \frac{1}{r^2} \frac{\partial^2f}{\partial \phi^2} + \frac{\partial^2f}{\partial z^2}$ |

### Integralsätze

#### Gauß'scher Satz

$$\iiint_V\nabla\cdot\vec{D}dV = \oiint_{A(V)}\vec{D}\cdot d\vec{A}$$

![Gauß'scher Satz](media/gaußscher_satz_1-4.png)

---

#### Stoke'scher Satz

$$\iint_A \nabla \times \vec{H} \cdot d\vec{A} = \oint_{C(A)} \vec{H}\cdot d\vec{r}$$

![Stoke'scher Satz](media/stokescher_satz_1-5.png)

---

## Maxwell Gleichungen

|  | Integral | Differentiell | kompl. Differentiell |
| --- | --- | --- | --- |
| 1. Ampere | $\int_C\vec{H}d\vec{x} = \int_F (\vec{J} + \dot{\vec{D}}) d\vec{F}$ | $rot \vec{H} = \vec{J} + \dot{\vec{D}}$ | $rot \vec{H} = \vec{J} + j\omega \vec{D}$ |
| 2. Induktion | $\int_C \vec{E}d\vec{x} = - \int_F$ |  | $rot \vec{\underline{E}} = -j\omega\vec{\underline{B}}$ |
| 3. Q als Quelle von $\vec{D}$ | | $div \vec{{D}} = \rho$ | $div \vec{\underline{D}} = \rho$ |
| 4. Fehlen magn. Ladung | | $div \vec{{B}} = 0$ | $div \vec{\underline{B}} = 0$ |

## Grundgeleichungen Elektromagentische Felder

Reell:

$\vec{D} = \varepsilon_0 \varepsilon_r \vec{E}$ ;  $\vec{B} = \mu_0 \mu_r \vec{H}$ ;  $\vec{J_{cond}} = \kappa \vec{E}$

komplex:

$\vec{\underline{D}} = \varepsilon_0 \underline{\varepsilon}_r (\omega) \underline{\vec{E}}$ ;  $\vec{B} = \mu_0 \mu_r \vec{H}$ ;  $\vec{J_{cond}} = \kappa \vec{E}$ **TODO (5-10)**

## Definition von Integralen und Dichten

> s. 1-7 in [StudIP](https://e-learning.tuhh.de/studip/sendfile.php?type=0&file_id=2b77f4b05cabd886c81016611aed67d5&file_name=%5B01%5D+Review+of+Maxwell%27s+Theory.pdf)

## Klassifikation Elektromagnetischer Felder

| Feldart | Beschreibung | Vereinfachungen |
| --- | --- | --- |
| Volldynamisch | Sich schnell änderne Felder; Nötig zur beschreibung von EM Wellen; Starkte Kopplung zw. E- und H-Feld | Keine Vereinfachungen. Alle MW Gleichungen gelten in vollem Umfang |
| Quasistationär | $\omega << ?$; Ausreichend für sich langsam ändernde Felder bzw. Felddiffusion; Grundlage für Skineffekt und Wirbelströme | Verschiebungsstrom kann vernachlässigt werden ($\dot{\vec{D}} \approx 0$) |
| Stationär | Induzierte Spannung vernachlässigt;  Loose EM Kopplung ($E \sim J$); Ausreichend für stationäre Stromdichten | Alle Zeitlichen Abhängigkeiten von Feldern gleich Null |
| Statisch | Ausreichend für statische Felder (Konst. Ladungen/Potentiale, ...); Grundlage Elektro- und Magnetostatik  | Kein Leitungsstrom; Kein Feld Zeitabhängig |

## Quasistationäre Felder und Induktion

### Faraday'sches Gesetz und Lentz'sche Regel

Das **Faraday'sche Gesetz** gibt die Induzierte Spannung $u_{ind}$ an.

$$ u_{ind} = \oint_C \vec{E}\cdot d\vec{r} = - \frac{d\Phi}{dt} = - \frac{d}{dt} \iint_A \vec{B}\cdot d\vec{A}$$

Nach der **Lentz'schen** Regel wirkt die induzierte Spannung bzw. der dadurch fließende Strom seiner Ursache entgegen. Das heißt, dass der durch induktion entstandene magnetische Fluss dem induzierenden entgegengesetz ist.

### Arten von Induktion

Man unterscheidet zwischen Externer und interner Induktivität.

### Gründe für Induktion

**TODO**

## Felddiffusion

**Felddiffusion** spielt auf zwei verschiede Weisen in der TET eine wichtige Rolle:

- 

## Ebene Wellen

Eine **Ebene Welle** ist ein Vektorfeld konstanter Frequenz, welches Folgende Bedingungen erfüllt:

- Lösung von Helmholtz und Maxwell Gleichungen
- Hat unendlich viele Ebenen als Wellenfront (Ebenen gleicher Phase)
- Ausbreitungsrichtung steht senkrecht auf den Ebenen gleicher Phase

Allgemeine Form:
$$\underline{\vec{E}} (\vec{r},t) = \underline{\vec{E}}_0 \cdot e^{j(\omega t - \underline{\vec{k}}\cdot\vec{r})} $$

### Leistung einer ebenen Welle

Die Leistung einer Ebenen Welle kann über den Pointing Vektor ausgedrückt werden:

$$\underline{\vec{S}} = \underline{\vec{E}} \times \underline{\vec{H}}^* = \frac{1}{2} \underline{\hat{\vec{E}}} \times \hat{\vec{\underline{H}^*}} $$