# Vergangene Midterms Themen CSTD

## Aufgabe 1: Stabilitätsanalyse

- Ist System Stabil? -> Eigenwerte bestimmen. Wenn alle in LHE ist system stabil
- Ist System Controllable?
- Ist System Observable?

### Theorie

- Stabilität (Stable, marginally stable): Eigenwerte in LHE
- Controllability:
  - Aufstellen der Controllability Matrix $\mathcal{C}=[b\;Ab\;...\;A^{n-1}b]$
  - System ist controllable, wenn $\mathcal{C}$ vollen Rang hat $\lrArr$ $\text{det}(\mathcal{C}) \neq 0$
  - Wenn system controllable ist, können Pole frei verschoben werden und das System kann zu jedem Punkt im State-Space gefahren werden
- Obervability:
  - Aufstellen der Observability Matrix $\mathcal{C}=[c\;cA\;...\;cA^{n-1}]^T$
- Stabilizability (using state feedback):