# Theorems and Definitions: Control System Theory and Design

# Chapter 1: Introduction

# Chapter 2: State Space Models

## Definition 2.1

An unforced system x˙(t) = Ax(t) is said to be stable if for all
x(0) = x0, x0 ∈ IR we have x(t) → 0 as t → ∞.