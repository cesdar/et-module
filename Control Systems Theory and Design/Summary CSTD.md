# Summary CSTD

# Chapter 1: Introduction

# 2.1: From TF to SS Model

# Chapter 2: State Space Models

# Chapter 3: Controllability and Pole Placement

# Chapter 4: Observability and State Estimation

# Chapter 5: Observer-Based Control

# Chapter 6: Multivariable Systems

# Chapter 7: Digital Control

# Chaoter 8: System Identification

# Chapter 9: Model Order Reduction

# Chapter 10: Case Study