# Formelsammlung CSTD

## Glossary

- TF: Transfer Function
- SS: State Space (model)

## Matrix calculations

## Eigenvalues and Eigenvectors

Eigenvalues can be calculated using the characteristic polynomial:

$$
\text{det}(A-\lambda I) = 0
$$

### Adjugate

The adjugate of $A$ is the transpose of its cofactor matrix.

$$
\text{adj}(A) = C^T
$$

Further it holds true:

$$
A^{-1}=\frac{1}{\text{det}(A)}C^T
$$

## Control System Theory Basic

### Dynamic behaviour of a system

A systems behaviour could be classified by evaluation of the TF's poles an zeroes. The poles are values of $s$ that lead to a zero in the denominator of $G(s)$ and zeros are zeros of the nominator of $G(s)$.

## Transfer Function

A Transferfunction has this form:
$$
G(s) = \frac{N(s)}{D(s)} = \frac{b_ms^m+b_{m-1}s^{m-1}+...+b_1s + b_0}{s^n+a_{n-1}s^{n-1}+...+a_1s+a_0}
$$

$\text{deg}(N(s))=m$ and $\text{deg}(D(s))=n$

### Proper TF

The TF of a system could be classified by the degree of its nominator and denominator:
- proper: $m\leq n$
- biproper: $m=n$
- strictly proper: $m<n$

## State Space Model

A MIMO system could be described as state space model:
$$
\dot{x}(t)=Ax(t)+Bu(t)
$$
$$
y(t)=Cx(t)+Du(t)
$$
with:
- $x(t) \in \R^n$: state vector
- $y(t)\in\R^l$: output vector
- $u(t)\in\R^m$: input vector
- $A\in\R^{n\times n}$: system matrix -> $\text{eig}(A)$ determine dynamic properties of system
- $B\in\R^{n\times n}$: input matrix
- $C\in\R^{l\times n}$: output matrix
- $D\in\R^{l\times m}$: feedthrough matrix

Poles of the TF are in SS model the eigenvalues of system matrix $A$.

### TF to SS

### SS to TF

Under the assumtion that $n>m$:
$$
G(s) = \frac{c\text{adj}(sI-A)b}{\text{det}(sI-A)}
$$