# Elektronische Bauelemente Formelsammlung

Formelsammlung von Julian Dieskau

Zu finden hier: https://writemd.rz.tuhh.de/TZlMHl8lQ3maHUrLHUgDYA

## Formelzeichen

| Zeichen | Einheit | Beschreibung |
| :-: | :-: | - |
| $N_D$, $N_A$ | $\frac{1}{cm^3}$ | Anz. Dotieratome pro cm³ |
| $U_T = \frac{kT}{q}$ | V | Temperaturspannung. ~25mV bei Raumtemperatur |
| $\mu_n$, $\mu_p$ | $\frac{cm^2}{Vs}$ | Elektronen- bzw. Löcherbeweglichkeit |
| $\rho = R\frac{A}{l}$ | $\Omega cm$ | Spezifischer Widerstand |
| $\sigma=\frac{1}{\rho}$ | $\frac{1}{\Omega cm}$ | Spez. Leitfähigkeit |
| k | $\frac{J}{K}$ | Boltzmann Konstante: $1,38\cdot 10^{-23}J/K$

## 1. Der homogene Halbleiter

### Was ist ein Kristall?

Periodisches Gitter + Basis = Kristallstruktur

Das Gitter ist ein Satz von Regeln für Translationen, die Basis das Objekt, welches verschoben wird.

### Dotierung

Dotierung ist einbringen von Fremdatomen, welche Störstellen darstellen und die Elektronen- bzw. Löcherkonzentration im Leitungs- bzw. Valenzband sowie den Abstand zum Fermi-Niveau verändern.

**P-Dotierung:** Valenzbandkante kommt näher an Ferminiveau
**N-Dotierung:** Leitungsbandkante kommt näher an Fermi Niveau 

### Effektive Zusatandsdichte

$$
\text{Leitungsband:}\quad N_L = 2(\frac{2\pi m_L k_b T}{h^2}) = 2,385\cdot 10^{19}\cdot (\frac{m_L}{m} \frac{T}{T_0})^{3/2} cm^{-3}
$$

$$
\text{Valenzband:}\quad N_V = 2(\frac{2\pi m_V k_b T}{h^2}) = 2,385\cdot 10^{19}\cdot (\frac{m_V}{m} \frac{T}{T_0})^{3/2} cm^{-3}
$$

Mit Effektiver Elektronenmasse $m_L$ und Löchemasse $m_V$. Für Silizium gilt: $\frac{m_L}{m_e} = 1,08$ und $\frac{m_V}{m_e} = 0,55$ ggf. andere Werte In aUfgabenstellung.

### Besetzungswahrscheinlichkeiten

Die Wahrscheinlichkeit, dass ein Zustand im Valenz- oder Leitungband besetzt ist wird mit der **Fermi-Dirac-Verteilung** beschrieben. Diese hat die Form:

$$
f_{FD}(W) = \frac{1}{1+\exp(\frac{W-W_F}{k_BT})}
$$

Als Verinfachung kann die **Boltzmann-Verteilung** verwendet werden, wenn gilt:
- Ferminiveau in Bandlücke liegt
- Abstand zu Bandkanten beträgt einige $k_BT$ (bei 300K $\approx0,023eV$)

:question: Wie viele k_BT ungefähr?

Bei dieser fällt die 1+ vor dem Exponentialterm weg:

$$
\text{Boltzmann Verteilung:}\quad
f_B(W) = \exp(\frac{W_F-W}{k_BT})
$$

### Elektronen- und Löcherkonzentration im Leitungs- und Valenzband

Unter den Bedingungen für Boltzmann Verteilung gelten näherungsweise folgende Formeln:

$$
\text{Elektronen im Leitungsband: }\quad
n=N_L\exp(-\frac{W_L-W_F}{k_BT})$$$$
\text{Löcher im Valenzband:}\quad
p=N_V\exp(-\frac{W_F-W_V}{k_BT})
$$

### Berechnung $\Delta W$ Abstand Leitungsband zu Ferminiveau

$$
\Delta W=-k_BT\ln\frac{n}{N_L}
$$

### Massenwirkungsgesetz

Im Thermodynamischen Gleichgewicht gilt:

$$
n\cdot p = N_LN_V\exp(-\frac{W_L-W_V}{k_BT})=n_i^2
$$

Für Intrinsichen, d.h. undotierten HL gilt: $n=p=n_i$

### Minoritätsträgerlebensdauer ($r_{th}$)

Durch z.B. Starhlung in Form von Licht kann der HL aus dem Thermischen Gleichgewicht gebracht werden. Die Lebensdauer der dadurch generierten Minoritätsladungsträger wird durch $\tau_n$ bzw. $\tau_p$ ausgedrückt. (s. auch Folie 1-81).

$p_0$ bzw. $n_0$ sind Ladungsträgerkonzentration im Thermodynamischen Gleichgewicht.

Bei n-Leitung gilt:
$$
p_0\ll n_0; \quad 
p_0 = \frac{n_i^2}{N_D}=\frac{n_i^2}{n_0}; \quad
r_{th}=\frac{p-p_0}{\tau_p}
$$

Bei p-Leitung gilt:
$$
n_0\ll p_0; \quad 
n_0 = \frac{n_i^2}{N_A}=\frac{n_i^2}{p_0}; \quad
r_{th}=\frac{n-n_0}{\tau_n}
$$

$r_{th}$ ist **Thermischer Rekombinationsüberschuss** und gibt an, wie viel mehr Trägerpaare pro Zeiteinheit rekombinieren als erzeugt werden.

$r_{th} > 0$ : Rekombination;  $r_{th}<0$ : Generation überwiegt

### Leitfähigkeit eines Halbleiters

Im allgemeinen lässt sich die **Leitfähigkeit eines HL** durch folgende Formel berechnen:
$$
\sigma=q(\mu_nn + \mu_pp)=\frac{1}{\rho}; \quad \left[ \frac{1}{\Omega cm}\right]
$$ 
Bei Dotierung kann jeweils der Term der Minoritätsträger vernachlässigt werden.

**Widerstand eines Halbleiters** lässt sich berechnen über:
$$
R=\rho\frac{l}{A} = \frac{l}{\sigma A} = \frac{l}{---}
$$

### Ladungsträgergeneration durch Bestrahlung

Energie eines Photons:

$$
W_P = hf = \frac{hc}{\lambda}
$$

Minimale Energie, die Ein Photon haben muss, um ein Elektron aus Valenz- ins Leitungsband zu heben ist Bandlücke $\Delta W = W_L-W_V$ .

## 2. Der pn-Übergang

### Diffusionsspannung

$$
U_D = \varphi(-\infty)-\varphi(+\infty) = \frac{kT}{q}\ln\frac{N_DN_A}{n_i^2} = U_T\ln\frac{N_DN_A}{n_i^2} % \frac{q}{2\varepsilon\varepsilon_0}(N_Dx_n^2 + N_A x_p^2)
$$

### Schockley-Gleichung

Gesamtstromdichte eines pn-Übergangs (**Schockley-Gleichung**):
$$
J=J_n+J_p=J_0\left( \exp\left(\frac{U}{U_T}\right)-1\right)
$$

Mit $J_0$ als **Sättigungsstrom**, der in Sperrichtung fließt und sich folgendermaßen ergibt:

$$
J_0 = J_{n0} + J_{p0} = \frac{\mu_nkT}{L_n}\frac{n_i^2}{N_A} + \frac{\mu_pkT}{L_p}\frac{n_i^2}{N_D} = {kTn_i^2}\left(\frac{\mu_n}{L_nN_A} + \frac{\mu_p}{L_pN_D}\right)
$$

Wenn $L_n=L_p$ gilt kann man das ganze noch folgendermaßen vereinfachen:

$$
J_0 = \frac{kTn_i^2}{L}\left(\frac{\mu_n}{N_A} + \frac{\mu_p}{N_D}\right)
$$

### Diffusionslänge

$$
L_p^2=\frac{\mu_pkT}{q}\tau_p=\mu_pU_T\tau_p=D_p\tau_p\\
L_n^2=\frac{\mu_nkT}{q}\tau_n=\mu_nU_T\tau_n=D_n\tau_n
$$

### Neutralitätsbedinging

Da Ladungen rechts und links der Sperrschicht sich ausgleichen müssen gilt:

$$
N_A x_P = N_D x_n
$$
$$
N_A + n = N_D + p
$$

Ebenfalls muss das E-Feld an der Sperrschicht stetig und gleich 0 sein.

### Breite der Sperrschicht einer pn-Diode

Die Breite der Sperrschicht eines pn-Übergangs $w$ lässt sich aus der Summe des Teils in der p- und n- Schicht berechnen. Dabei ist $U$ die in Flussrichtung angelegte Sapnnung.

$$
w_p = \sqrt{\frac{2\varepsilon N_D(U_D-U)}{qN_A(N_A+N_D)}}; \quad
w_n = \sqrt{\frac{2\varepsilon N_A(U_D-U)}{qN_D(N_A+N_D)}}
$$ $$
w = w_p + w_n = w_p = \sqrt{\frac{2\varepsilon (U_D-U)}{q} \left(\frac{1}{N_A}+ \frac{1}{N_D} \right)}
$$ 

Die maximale elektrische Feldstärke in der Sperrschicht berechnet sich durch:

$$
|E_{max}| = -E(0) = \sqrt{\frac{2 q N_A N_D (U_D-U)}{\varepsilon(N_A + N_D)}} = \frac{2(U_D-U)}{w}
$$

Das E-Feld ist negativ beim pn-Übergang und positiv beim np-Übergang.

### Sperrschichtkapazität

Ladung auf Sperrschichtkapazität einer Sorte:

$$
Q(U) = qAw_nN_D
$$

Sperreschichtkapazität:

$$
c_s = A\sqrt{\frac{\varepsilon qN_DN_A}{2(N_A+N_D)(U_d-U)}} = \varepsilon\frac{A}{w(U)}
$$

Daraus lässt sich ableiten, dass $c_s \to \infty$ für $U\to U_D$

## 3. Der Bipolar Transistor

### Kennliniengleichungen
![](https://wikimedia.org/api/rest_v1/media/math/render/svg/59f5873e17d0435935e3ebee078dec1b53e42fc9)

#### pnp - Transistor

$$
I_E = I_{F0} (\exp{\frac{U_{EB}}{U_T}}-1) - \alpha_RI_{R0}(\exp{\frac{U_{CB}}{U_T}}-1)
$$

#### Vereinfachungen im Aktiv-Normalbereich

Im Aktiv-Normalbereich ist Emitter-Basis Strecke in Durchlass- und Kollektor-Basis Strecke in Sperrrichtung gepolt.

Im Allgemeinen gilt: 
$$
|U_{BE}| \gg U_T \quad\text{und}\quad |U_{CB}| \gg U_T
$$

Diese Bedingung führt dazu, dass in der Kennliniengleichung der Betrag des Arguments der Exponentialfunktion sehr groß wird. Somit kann, wenn dieses Argument negativ ist, der Exponentialterm vernachlässigt werden.

## 4. Unipolare Bauelemente

### Schottky Kontakt

$$
\text{Weite der RLZ:}\quad
w=\sqrt{\frac{2\varepsilon}{qN_D}(U_D-U)}
$$

$$
\text{Elektrisches Feld:}\quad
|E(x)|=\frac{qN_D}{\varepsilon}(w-x)
$$

### Feldeffekt

Durch ein veränderbares elektrisches Feld werden freie Ladungsträger in einem halbleitenden Material verdrängt oder angezogen, wodurch die elektrische Leitfähigkeit verändert wird. Ein Stromfluss kann somit nur durch das Umladen von Ladungsträgern beeinflusst werden. Im Gegensatz zum  Bipolartransistor wird am Steuerkontakt (Gate) nur bei der Änderung des Feldes Leistung benötigt, wenn man die Verluste vernachlässigt.

### Dotierungsverhältnisse am MOSFET

| Anschluss | Dotierung n-Mos | Dotierung p-Mos |
| :-: | :-: | :-: |
| Source/Drain | n+ | p |
| Bulk | p | n |
| Gate | Metall/PolySi | Metall/PolySi|
| Ladungsträger | Elektronen | Löcher |

### MOSFET Schwellenspannung

Die Gate Spannung, bei der Inversion eintritt, also $\psi_S=2\psi_B$ gilt, heißt **Schwellenspannung** und kann wie folgt berechnet werden: 

$$
U_{th} = U_{oxT}+2\psi_B +\phi_{ms}=\frac{Q'_{SD}}{C_{ox}}+2\cdot U_T \ln\left(\frac{N_B}{n_i} \right)+\phi_{ms}
$$

$\phi_{ms} = U_{FB}$ ist hierbei die Austrittsarbeitsdifferenz zwischen Halbleiter und Metall (oder Poly Si). :question:: $N_B$ Ist durch N_A bei n-Dotierung und N_D bei p-Dotierung zu ersetzen.