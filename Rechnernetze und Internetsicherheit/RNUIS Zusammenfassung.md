# Rechnernetze und Internetsicherheit

## Kapitel 1

### Einleitung

- Viele vebundene Geräe -> Hosts
- Geräte, die das Netzwerk zu einem solchen machen -> Packet Switches
- Verbindungen -> Faser, Kupfer, Funk, ...
  - Das Internet ist ein Netzwerk aus Netzwerken
- Protokolle sind überall in einem Netzwerk (HTTP, TCP, IP, WiFi, ...)
- Internet bietet ein INterface, welches Anwendungen nutzen können, um Nachrichten zu übertragen. Hierfür werden für verteilte Anwendungen Programmierschnittstellen bereitgestellt.

### Was ist ein Protokoll?

Beispiel: "Wie spät ist es?" Protokoll:
- Einfacher Aufbau:
  1. A: "Hi"
  2. B: "Hi"
  3. A: "Wie Spät ist es?" Anfrage (Request)
  4. B: "Es ist 13:01" Antwort (Reply)

Netzwer Protokolle sind sehr ählich. Hier sind die Kommunizierenden nicht Mesnchen sondern Computer oder Geräte, welche sich an spezifizierte Protokolle halten.

Analogie zum "Wie Spät ist es?" Protokoll: HTTP
1. Client: TCP Connection request
2. Server: TCP Connection response
3. Client: `GET http://www.mydomain.com`
4. Server: `<Datei>`

**Definition:** 
> **Protokolle** definieren das Format, die Reihenfolge von Nachrichten, die über das Netzwerk gesendet werden und die Handlungen, die bei Nachrichtenempfang ausgelöst werden.

### 1.2 The Network Edge

- **Host**: Clients und Server
- **Server**: Häufig in Rechenzentren
- Wie verbindet man ein ENdsystem mit einem Edge Router?
  - Heimnetzwerk, Unternehmensnetzwerk, mobiles Netzwerk (4G, WiFi), ...
- **Wichtige Kenngrößen**: Übertragungsrate (Transmission rate in bits/s) des access Netzwerks; Geteilter oder dedizierter Zugriff auf Netzwerk

#### Access Networks: Kabelinternet

- Ein physisches Kabel verbindet mehrere Haushalte zum "cable headend"
- Signale der einzelnen Nutzer werden mit verschiedenen Frequenzen übertragen -> **frequency division multiplexing (FDM)**
- meist Asymmetrisch -> Downloadrate größer als Uploadrate

#### Access Networks: DSL (digial subscriber line)

- Nutzung bestehender Telefon Infrastruktur -> twisted pair
- exclusiive Nutzung des Kabels und kein teile mit Nachbarn

#### Wireless access networks

- Lokale: WiFi; Wide-Area: Cellular Networks (4G/5G)

### Pakete

Host: Nimmt Anwendungsdaten und teilt sie in kleinere Happen (chunks) auf, welche dann **Pakete** der Länge **L** gennant werden. Dann wird das Paket mit der **Übertragungsrate (transmission rate) R** über einen Link mit einer **Kapazität bzw. link Bandbreite** übertragen.

$$
\text{txDelay} = \frac{L\text{(bits)}}{R\text{(bits/s)}}
$$

#### Eigenschaften Physischer Medien

- **bit:** wird übertragen zwischen Sender und Empfänger
- **physischer Link:** Was zwischen sender und Empfänger liegt
- **guided media:** Festes Medium in dem Signale sich ausbreiten können
- **unguided media:** Signale können sich frei im Raum ausbreiten

#### Beispiele physischer Medien

- Twisted Pair: DSL, Ethernet, ...
- Coax Kabel: Zwei konzentrische Leiter: Kabelinternet
- Galsfaser: Nutzen Lichtpulse und erreichen sehr hohe Bandbreiten und Reichweiten
- Drahtlos: Signal wird in verschiedenen Bänder oder auch Frequenzbereichen übertragen.
  - Kein physiches Kabel
  - Broadcast
  - half-duplex
  - Ausbreitungseffekte wie Reflektionen, verdeckung und Interferenz müssen berücksichtigt werden

### 1.3 The Network Core

- Netz aus miteinander verbundnen Routern
- **Packet-Switching**: Host setzt Anwendungsdaten in Packete um und Netzwerk leitet Paktet über verschiedne links von der Quelle zum Ziel
- Zwei grundlegende **network-core functions**
  1. **Forwarding**: aka. switching. Lokale weiterleitung von Paketen von Router Eingangslink zu richtigem Router Ausgang
     - Umgesetzt durch Forwarding Tabelle, welche durch Routing Algorithmus bestimmt wird
  2. **Routing**: Globale Bestimmung des Pfades, den ein Paket zurücklegt -> **Routing Algorithmen**

#### Packet-Switching: store-and-forward

- komplettes Paket muss empfangen werden, befor dieses weitergeleitet wird
- Wenn Eingangsbandbreite an Router höher als Ausgangsbandbreite kann die queue überlaufen -> **Paketverlust** 

### Circuit-Switching

- Alle Ressourcen (Links) werden für einen einzigen "Anruf" exklusiv reserviert
- Vorteil: Keine Switching delay; Keine Verluste
- Nachteile: Ressourcen können IDLE gehen und ist dann verloren
- Mögliche Circuit Switching Ansätze:
  - **Frequency Division Multiplexing (FDM)**: Jeweils eine gewisse Frequenz steht einem Teilnehmer exklusiv zur Verfügung
  - **Time Divison Multiple Access (TDM)**: Einzelne Zeitschlitze stehen Teilnehmer exklusiv zur Verfügung

### 1.4 Performance

- Packet Delay:
  1. **Processing Delay**: Delay durch Forwarding Table Lookup, Switching, Integrity Checks -> us
  2. **Queueing Delay**: Warten, dass output link für Übertragung frei wird -> Hängt von Auslastung des Ausgangslinks ab
  3. **Transmission Delay**: Zeit die zur tatsächlichen Übertragung benötigt wird -> Bandbreite mal Bits
  4. **Propagation Delay**: Physiklische Begrenzung der Ausbreitungsgeschwindigkeit von Infprmationen über Leitungen

Echten Delay pro Router kann man messen mit `traceroute www.google.com` bzw. unter Windows `tracert www.google.com`

[Analogie für Delays](https://youtu.be/hm1y4LsphQQ?t=196)

#### Packet Loss

- Queue eines Links hat endliche Kapazität und kann überlaufen
- Pakete, welche bei einer vollen Queue ankommen werden fallengelassen bzw. gehen verloren
- Verlorene Pakete können von vorhergehendem Node erneut übertragen werden, können aber auch einfach ohne Info verloren gehen

#### Throughput

- Rate (bit/s) mit der Bits gesendet wertden -> kann momentan oder gemittelt sein
- Analogie: Wasser in einer Röhre
  - Querschnitt bestimmt Kapazität des Links
  - Alle Clients senden "Wassser" in Netzwerk
- Maximaler Durchsatz ist limitiert durch schwächsten Link -> Bottleneck Link

### 1.5 Layering


## Kapitel 2: The Application Layer

### 2.3 Email

- Verteiltes System, welches aus drei Komponenten besteht:
  - User Agents: Email Programm, Benutzer
  - Mail Server
    - Hat eine Mailbox pro User
    - Hat eine Message Queue mit zu sendenden Mails
  - Simple mail transport Protocol: SMTP
    - Protokoll, welches verwender wird, um Mails vom Benutzer oder einem Server zu einem anderen Server zu pushen
    - Client-Server-Paradigma

#### Beispielhafte Mailübertragung

[Szenario: Alice sendet Bob eine E-mail](https://youtu.be/D3GMrOMR2dk?t=154)

1. Alice benutzt Mailprogramm um Mail an ("to") bob@someschool.edu zu schreiben
2. Alice's Mailprogramm sendet die Mail an ihren Mailserver über SMPT und diese wird in der Mesage Queue des Servers eingeordnet
3. Alice's Mailserver öffnet als Client eine TCP Verbindung mit Bob's Mailserver
4. Alice's Mail wird über SMTP an Bob's Server über den TCP Kanal übertregen
5. Bob's Mailserver packt Alice's Mail in Bob's Postfach
6. Bob ruft seine Mails mit seinem Mailprogramm ab

#### SMTP

- Benutzt TCP mit Port 25 auf der Server Seite
- 3 Phasen
  - SMTP Handshake
  - SMTP Übertragung
  - SMPT Beendigung
- Ähnlich zu HTTP


```SMTP
S: 220 hamburger.edu
C: HELO crepes.fr
S: 250 Hello crepes.fr, pleased to meet you
C: MAIL FROM: <alice@crepes.fr>
S: 250 alice@crepes.fr... Sender ok
C: RCPT TO: <bob@hamburger.edu>
S: 250 bob@hamburger.edu ... Recipient ok
C: DATA
S: 354 Enter mail, end with "." on a line by itself
C: Do you like ketchup?
C: How about pickles?
C: .
S: 250 Message accepted for delivery
C: QUIT
S: 221 hamburger.edu closing connection
```

Abrufen von Mails über IMAP oder POP3

## Glossar

