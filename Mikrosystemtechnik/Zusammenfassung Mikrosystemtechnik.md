# Zusammenfassung Mikrosystemtechnik

## Introduction and Fundamentals

## Scaling

### Surface-to-Volume Ratio

Die Ausprägung physikalischer Effekte ist stark von der Größe einer Struktur abhängig. So muss eine Brücke aus Beton mit den Dimensionen 50x10x0,35m durch zusätzliche Strukturen wie Brückenpfeiler gestützt werden, eine MEMS Struktur mit den Abmessungen von 50x10x0,35µm hingegen kann aber komplett frei stehen. Hier ist der Einfluss der Masse bzw. Graviation im Verhältnis zu den Bindungskräften der ausschlaggebende Faktor. Die Makro-Brücke wiegt 420t im Gegensatz zur MEMS Brücke, welche nur 840 fg wiegt. Somit ist es notwendig in Mikro- und Nanosystemen Effekte zu beachten, welche in der täglich erfahrbaren Welt in der Makroscala meist vernachlässigt werden können. Auf der anderen Seite wirken sich Effekte der Makroscala teilweise deutlich schächer in Mikro- und Nanostrukturen aus.

Das wichtigste Verhälnis bei der Betrachtung von Scaling Effekten ist das Surface-to-Volume Ratio. Dieses gibt, wie der Name schon sagt, das Verhältnis der Oberfläche eines Körpers im Vergleich zu seinem Volumen an. Beim Scaling vieler physikalischer Eigenschaften ist dieses Verhältnis dominieren, was die Eigenschaftsveränderung angeht. Dazu zählen u.a.:
- Masse (Gravitation, Trägheit): weniger relevant für kleine Strukturen
- Wärme (Energie): gespeichert in Volumen, fließt durch Flächen
- Chemische Reaktionen: Finden an Oberflächen statt -> erhöhte reaktivität

### Beispiel: Würfel wird zerschnitten

Wir betrachten einen Würfel mit der Kantenlänge von 1mm. Das Volumen ergibt sich aus $V=(1mm)^3 = 1mm^3$. Die Oberfläche ist sechs mal die Oberfläche einer Würfelfläche, also $A = 6\cdot(1mm)^2$

### Flexible Structures

### Currents in Nanowires

### Interaction with waves

### Quantum effects

Quantum effcts, quantum well, Tunnel Effect, Interatomic Forces

### Characteristic Numbers

Derivation of characteristic numbers, Scaling and the EM Wave Equation, Froude Number and different characteristic numbers


## Fabrication Technologies

#### Processing Appoaches

### Fundamental Processing Techniques

- Substrat, besonders flach
- Ablagerung auf Oberfläche von zweitem Material
- Beschichtung mit Photoresist (Spin-Coating)
- Belichtung des Phtoresists
- Entwicklung Photoresist
- Ätzung
- Resist Stripping

### Special Processing Techniques

#### Lift-off Prozess

- Photoresist beschichtung, belichtung und entwicklung
- Ablagerung von Material auf gesamte Oberfläche
- Ablösung des abgeschiedenen Materials an nicht gewünschten Stellen zusammen mit Photoresist strippen

### Sacrificial Layer Etching

Nutzung zur fabrikation von freistehenden Strukturen.

- Ablagerung von Sacrificial Layer und Nutzlayer
- Lithographie und 

### Lab and Process Conditions

- Clean Room Classification
- Sources of Particles
- Principles of Clean Room Labs
- Required Process Conditions
- Basics of Vacuum Technology

### Materials

- Calssification of Materials
- Structural Properties of Crystals
- Classification of Materials
  - Conductors
  - Semiconductors
  - Dielectrics
  - Polymers
- Substrate Materials
- Thermal Oxidation of Silicon
- Wafer fabrication
  - Silicon
  - Crystallization
  - Crystal Growth
- Wafer Properties
- SOI Wafer

### Deposition (pp. 4.4)

- Condensation of Material
- CVD and PVD Processes: Chemical Vapor Deposition and Physical VD
- Evaporation
- PECVD: Plasma Enchanced CVD
- LPCVD: Low Pressure CVD
- Sputter Deposition
- Epitaxy
  - Band Gap Engineering
  - Moleculat Beam Epitaxy (MBE)
- Spin-Coating

### Lithography (pp. 5.3)

- Basic Process Steps
- Requirements and Concepts
- Lithography Masks
- Optical Lithography
  - Proximity lithography
  - Contact lithography
  - Projection lithography
  - Limits
  - Theory of Optical Image Formation
    - Abbe Limit and Rayleigh Criterion
    - Rayleigh Criterion
    - Resolution of optical Imaging (Incoherent)
  - Photo Resists - Exposure
  - Increase the Resolution of Lithography
  - Light Sources
  - Photo Resists
  - Photo Resists - Processing
- Electron Beam Lithography
- Nanoimprint Lithography

### Etching

- Fundamentals of Etching
  - Etching Process Properties
- Wet Etching - Crystals
- Dry Etching
  - Reactive Ion Etching (RIE)
  - Isotropic
  - Deep Reactive Ion Etching (DRIE)
- Combined Dry and Wet Etching

### Additional Processes

- Doping of Semiconductors
- Thermal Annealing
- Electrical Contacts
- Wire Bonding
- Wafer Fusion and Bonding
- Cleaning Processes
- Drying Processes
- Electroplating Process
- Laser Processing

### Fabrication Technology

- Planar Electronics
- 3D Electronics
- MEMS Devices
- Piezoelectric Micromechanical Resonator
- MEMS Devices

## Stress and Strain IN Microsystems

- Stress: Force which is contained in a mechanical body
- Strain: elastic deformation of the mechanical body
- Strain is the effect caused by stress

### Stress in MEMS Devices

### Fundamentals

- Origin of Stress
- Types of Stress
  - Thermal expansion
- Material Properties
- Thermal Dependence

### Further:

- Biaxial Stress
- Anisotropy
- Wafer Bending
- Stoney's Equation
- Stress Distribution Near Edges
- Stress in 3D
- Stress in Multilayer Structures
- Tailored Stress & Compensation
- Characterization of Stress
- Effects of Stress
- Stress in Metal Layers
- Creep and Stress Migration
- Stress Relaxation
- Micro-Origami
- "Elephant" couples

## Actuators

### Fundamentals of Actuation

### Magnetic Actuation


## MEMS Devices

## Yield, Failure, Reliability

## Optical Microsystems & Nanotechnology
