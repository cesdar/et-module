# Praktikum Hochfrequenztechnik

## Versuch 1

### Nomenklatur

- Phasenkoeffizient $\beta_i$
- Stoffwellenzahl $k_0=\omega \sqrt{\mu \epsilon}$
- Grenzwellenzahl $ k_{ci} $

### Zusammenfassung Kapitel 1

- Impedanz- und Admittanzmatrix sind für Mikrowellenschaltungen nur mäßig gut zu gebrauchen
- Besser: Beschreibung durch komplexe Amplituden von hin- und rücklaufenden Eigenwellen eines Wellenleiters
  - Können z.B. bestimmt werden aus: VSWR, Position der Feldminima, transportierte Leistung
  - Beschreibung über Eigenwellen führ zu Streumatrizen: **S-Parameter**
- Annahmen für Mikrowellen-Schaltungselemente in vielen Fällen:
  - Verlustlos und Reziprok: $\alpha=0$; $s_{11}=s{22}$
  - Verlustlos und Symmetrisch: 

### Zusammenfassung Kapitel 2: Theorie

- Annahme Rechteckhohlleiter mit ideal leitenden Wänden und homogener, isotroper, verlustloser Stofffüllung
- Bedingung für ausbreitungsfähige Eigenwellen:
$$
    \beta_i^2 = k_0^2-k_{ci}^2 > 0 \text{ bzw. } k_{ci} < k_0
$$
- bei Anregung von irgendeinem Hohlleiter Querschnitt mit beliebiger Feldverteilung sind nach ausreichender Hohlleiter Länge nur noch Felder der ausbreitungsfähigen Eigenwellen vorhanden
- Hier nur Betrachtung ausbreitungsfähiger Eigenwellen
- Für Rechteckhohlleiter gilt:
$$
  k_{ci} = \sqrt{\left(\frac{m\pi}{a}\right)^2 + \left(\frac{n\pi}{b}\right)^2}
$$
- Koordinaten: 
  - x und y liegen in Hohlleiterwänden; Aufgespannte Ebene ist Querschnitt
  - z liegt in longitudinaler Richtung, also in Ausbreitungsrichtung der Wellen. Richtung zum Schaltungselement wird positiv gezählt.

### Aufgaben

1. Berechnen Sie die Streumatrix einer verlustlosen Leitung der Länge l, der Ausbreitungskonstanten β und des Wellenwiderstandes Z. Benutzen Sie dazu die Leitungsgleichungen in der Form:

Überprüfen Sie Ihr Ergebnis durch eine Transformation der Streumatrix eines unendlich kurzen Leitungsabschnittes bei geeigneter Verschiebung der Referenzebenen!

2. Bestimmen Sie die Streumatrix des in Bild 9 dargestellten TEM-Leitungs-Zweitores. Behandeln Sie die beiden Tore als ideal abgeschlossen und das Zweitor als verlustlos.
3. Skizzieren Sie die Stromverteilung auf den Wänden eines Rechteckhohlleiters, in dem sich eine H10-Eigenwelle ausbreitet. Wo kann in longitudinaler Richtung ein Schlitz angebracht werden, ohne diese Eigenwelle zu stören?
4. Berechnen Sie die Grenzfrequenz fc und die Ausbreitungskonstante β (f =17 GHz) der H10-Eigenwelle des in Bild 7 dargestellten Schaltungselementes
    a) für den leeren Hohlleiter.
    b) für den mit Dielektrikum gefüllten Hohlleiter.
5. Leiten Sie die Gleichungen (33) und (34) her. Benutzen Sie die Bedingungen für
verlustlose und symmetrische S-Matrizen. Wie lässt sich die Phasenunbestimmtheit
von nπ von S12 und S21 aus Gleichung (34) physikalisch interpretieren?
Reflexion auf Wellenleitern 16
6. Wie groß ist die Eingangsimpendanz ZE einer verlustlosen Leitung der Länge l mit
der Ausbreitungskonstanten β und dem Wellenwiderstand Z, wenn die Leitung mit
einem Abschlusswiderstand ZA abgeschlossen ist? Wie groß ist der Eingangsreflexionsfaktor rE dieser Leitung bezogen auf einen Wellenwiderstand Z0?
7. Berechnen Sie für die Frequenz f = 17 GHz den Streuparameter S11 des in Bild 7 dargestellten Schaltungselementes auf zweierlei Art und Weise. Zeichnen Sie dazu die entsprechenden Leitungs-Ersatzschaltbilder und transformieren Sie die Impedanz am Ende der Leitung an den Anfang.
    a) Berechnen Sie den Reflexionskoeffizienten am Eingang bei Anpassung am Ausgang mit Hilfe des Ergebnisses aus 6. und durch Verschiebung der Referenzebenen. Drücken Sie dazu das Verhältnis der Wellenwiderstände durch deren Phasenkonstanten aus
    b) Überprüfen Sie das Ergebnis anhand von Gleichung (35) mit den Reflexionskoeffizienten für Kurzschluss und Leerlauf in der Symmetrieebene. Wenden Sie zur Lösung den Zweiteilungssatz für symmetrische Netzwerke an.
1. Zeigen Sie, dass zwischen dem Stehwellenverhältnis s und dem Hohlleiterdämpfungskoeffizienten α folgende Beziehung gilt, wenn der Hohlleiter mit |r| = 1 abgeschlossen ist.

### Vorbereitung Lösung

#### Aufgabe 1: Streuparameter

- Gegeben ist verlustlose Leitung -> Reziprok, also $s_{11}=s_{22}$ und $s_{21}=s_{12}$
- Gegeben sind folgende Zusammenhänge:
$$
\underline{U_i}(z_i)=\sqrt{Z_i}(\underline{a_i}e^{-j\beta_i z_i}+\underline{b_i}e^{j\beta_i z_i})
$$

$$
\underline{U_i}(z_i)=\frac{1}{\sqrt{Z_i}}(\underline{a_i}e^{-j\beta_i z_i}-\underline{b_i}e^{j\beta_i z_i})
$$

- dabei ist $z_i$ jeweils der Abstand

## Versuch 2