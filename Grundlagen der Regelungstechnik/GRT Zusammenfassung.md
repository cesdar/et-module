# GRT Zusammenfassung

# Lineare Zeitinvariante Systeme

- LTI Systeme sind Systeme, welche für alle Zeiten $t$ die gleiche Impulsantwort haben

## Differentialgleichungsmodelle

## Laplace Transformation und Übertragungsfunktionen

Um Systeme leichter darstellen zu können gibt es die Laplace Transformation, welche eine Differentialgleichung in eine Frequenzbereichsdarstellung umwandelt.

Hier wird folgendes angenommen:\
$$x(t) = 0,     t<0$$

Die Laplace Transformation von $x(t)$ ist folgendermaßen definiert mit der Komplexen Variablen $s$:
$$\mathcal{L} [x(t)] = \int_{0}^{\infty} x(t)e^{-st}dt = X(s)$$

### Differentiation und Integration

Differention und Integration sind leicht mit der Laplace Transformation im Frequenzbereich durchzuführen. Einfache Zeitliche Integration führt zu einem Vorfaktor von $s$ und einfache Zeitliche Ableitung führt zu einem Vorfaktor $\frac{1}{s}$.

### Übertragungsfunktionen

Mit der Laplace Transformation ist eine leichtere Darstellung von Differentialgleichungen im Zeitbereich möglich. Es vereinfacht sich die DGL zu einer Algebraischen Gleichung.

$$\ddot{y}(t) + a\dot{y}(t) + by(t) + c = x(t)$$
kann transformiert werden zu:
$$s^2Y(s) + asY(s)  + bY(s) + c = X(s)$$

### Endwertsatz

Wenn ein Signal $x(t)$ gegen einen endlichen, konstanten Wert für $t \rightarrow \infty$ konvergiert, kann dieser Wert über die Laplace Transformation berechnet werden wie folgt:
$$\lim_{t\to\infty}x(t) = \lim_{y\to 0} sX(s) $$

> :exclamation: Dieser Grenzwert hat nur eine Bedeutung, wenn ein Linksseitiger Grenzwert existiert bzw. wenn $x(t)$  gegen einen endlichen, konstanten Wert für $t\to\infty$ konvergiert!

### Anfangswertsatz

Genauso wie der Endwert kann auch der rechtsseitige Anfangswert über die Laplace Transformation gewonnen werden:

$$ x(0^+) = \lim_{s\to\infty} s X(s) $$

### Faltungsintegral

Durch die Laplace Transformation fällt es uns leichter die Systemantwort mehrerer hintereinandergeschalteter LTI Systeme zu berechnen. Konkret vereinfacht sich hier eine Faltungsoperation zu einer Multiplikation. Somit wird aus:

$$x_1(t) * x_2(t) = \int_0^t x_1(\tau) x_2(t-\tau) d\tau $$

durch Laplacetransformation dies:

$$\mathcal{L} [x_1(t)*x_2(t)] = X_1(s)X_2(s) $$