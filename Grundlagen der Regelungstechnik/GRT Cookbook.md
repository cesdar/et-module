# GRT Cookbook

## Laplace Transformation

Wichitge Eigenschaften der Laplace Transformation:

| Zeitbereich | Laplace Trafo | Eigenschaft |
| :---: | :---: | :---:  |
| $\alpha_1x_1(t) + \alpha_2x_2(t)$ | $\alpha_1X_1(s) + \alpha_2X_2(s)$ | Linearität |
| $x(t-d)$ | $e^{sd}X(s)$ | Zeitverschiebung |
| $\frac{d^n}{dt^n}x(t)$ | s^nX(s)-... | Differentiation |
 | $\int_0^t x(\tau) d\tau$ | $\frac{1}{s}X(s)$ | Integration|
| $\lim_{t\to\infty} x(t)$ | $\lim_{s\to 0} s X(s)$ | Endwertsatz |
| $x(0^+)$ | $\lim_{s\to\infty} s X(s)$ | Anfangswertsatz |

## Sprungantwort (Step Response)

Die Sprungantwort eines Systems gibt an, wie sich das System verhält, wenn der Eingang von 0 auf 1 Springt. MATLAB: `step(G)`

Da der Einheitssprung im Frequnezbereich $\frac{1}{s}$ entspricht kann die Sprungantwort eines Systems wie folgt als Multiplikations im $s$-Bereich berechnet werden:

$$Y(s) = \frac{1}{s} G(s)$$

### Pole

Polstellen sind die Punkte einer Übertragungsfunktion, an denen ihr Betrag gegen $\infty$ strebt. Dies kann auch dadurch ausgedrückt werden, dass der Betrag des Nenners 0 wird. MATLAB: `pole(G)`

Die Lage der Polstellen in der Komplexen Ebene bestimmt das dynamische Systemverhalten wie folgt:

| Polart | Systemverhalten |
| --- | --- |
| rein negativ Reell | Direkter, weicher Übergang von 0 zu 1; Keine Schwingung; Exponentielle Abnahme |
| rein positiv Reell | exponentieller Anstieg füt $t>0$ |
| kompl. konj. LHE | Überschwingen aber abklingend |
| kompl. konj. mit $Re\{p_i\} = 0$ | Dauerschwingung |
| kompl. konj. RHE | instabile, anwachsende Schwingung |

![Polstellenverhalten](./media/poles_behaviour_graph.PNG)

### Statische Verstärkung (Static Gain)

Die Statische Verstärkung ist jenes Gain, welches sich für $t\to\infty$ einstellt. Diese kann über den Endwertsatz berechnet werden. MATLAB: `dcgain(G)`

$$\lim_{t\to\infty} y(t) = \lim_{s\to 0}sG(s)\frac{1}{s} = \lim_{s\to 0} G(s) $$

Wenn G(0) existiert (:exclamation:) ist dies die statische Verstärkung des Systems.

### Physikalische Realisierbarkeit

Um physikalisch realisierbar zu sein muss ein LTI System immer gleich viele oder mehr Polstellen als Nullstellen haben $n\geq m$. 

Anz. Polstellen: $n$
Anz. Nullstellen: $m$

Systeme mit $n\geq m$ heißen proper, Systeme mit $n\gt m$ heißen streng proper. Systeme mit $n=m$ heißen Biproper

Die Differenz zwischen Pol- und Nullstellen $n-m$ wird **Polstellenüberschuss** oder **Polüberschuss** genannt.

### Übertragungsfunktionen und Blockdiagramme

Reihenschaltung von Blöcken: Multiplikation der Übertragungsfunktionen\
Parallelschaltung von Blöcken: Addition der Übertragungsfunktionen\
Geschlossener Kreis: 
![Closed Loop Transfer funktion](./media/closed_loop_tf.png)
Hier ist die Sache schon ein bisschen komplizierter. Dies ist aber eine sehr häufige Struktur in der Regelungstechnik. Es ergibt sich:
$$Y_1(s) = G_1(s)U_1(s) = G_1(s) (R(s) + Y_2(s)) $$
$$ Y_1(s) = \frac{G_1(s)}{1-G_1(s)G_2(s)}R(s) $$
Genauere Herleitung s. Skript S. 22. Einfacher zu merken ist:
$$ \text{closed loop tf} = \frac{\text{forward gain}}{1-\text{loop gain}} $$

### Systeme 2ter Ordnung (ab S. 24)

Systeme zweiter Ordnung können u.a. durch folgende Parameter charakterisiert werden:
- statische Verstärkung $ K = G(0) $
- Eigenfrequenz (natural frequency) $\omega_n = \sqrt{a_0} $
- Dämpfungsfaktor $\zeta = \frac{a_1}{2\omega_n}$

Über die gegebenen Definitionen kann ein System 2. Ordnung so geschrieben werden:
$$ G(s) = K\frac{\omega_n^2}{s^2 + 2\zeta\omega_ns + \omega_n^2} $$

### Dämpfungsfaktor

Folgende Werte kann der **Dämpfungsfaktor** annehmen:

- $\zeta=0$: Transiente Systemantwort ist eine reine, ungedämpfte Schwingung, die zu einem Polpaar auf der Imaginären Achse korrespondiert
- $\zeta \geq 1$: Exponentieller Abfall, der durch reelles Polpaar bestimmt wird.
- $0 \lt \zeta \lt 1$: Systemantwort enthält sowohl Schwingung als auch Abfall. Die Geschwindigkeit des Abfalls wird durch den Realteil von $-\zeta$ der Pole und die Frequenz durch den Imaginärteil $\pm j\sqrt{1-\zeta^2}$ bestimmt.

Eine Dämpfungskonstante von $\zeta = 1$ heißt kritische Dämpfung, da es genau der Übergang von Oszillierendem und reinem exponentiellen Abfall ist. In der Praxis sind oft Dämpfungen bis ~0,7 noch akzeptabel.

## Eigenfrequenz (ab S. 26)
**TODO**

## Kapitel 4: Zeitbereichs Spezifikationen

### Peak Overshoot

$$M_p = \frac{y_{max}-y_{ss}}{y_{ss}} = e^{-\frac{\pi\zeta}{\sqrt{1-\zeta^2}}} \approx 1 - \frac{\zeta}{0.6} $$

### Rise Time (Anstiegszeit)

Zeit, die Benötigt wird, um von 10% bis auf 90% des Steady State Wertes zu kommen.

### Ausregelzeit (Settling Time)

Zeit, die vergeht bis die Abweichung vom Steady State Fehler dauerhaft kleiner als 1% ist:

$$ \left\vert{\frac{y(t) - y_{ss}}{y_{ss}}}\right\vert \lt 0.01, \quad \forall t \geq t_s $$

Annähernd kann man annehmen:
$$ t_s \approx \frac{4.6}{\zeta\omega_n} = \frac{4.6}{\sigma} $$

**Stehengeblieben S. ~30/59**

### Nyquist Stabilitätskriterium

## Kapitel 3: Wurzelortskurven Design

### Zeichnen einer WOK (Skript S. 83 ff)

MATLAB: `rlocus(G)`

1. Open-Loop Pole und Nullstellen einzeichnen
   - Open-Loop Pole können auch als Pole/NS des geschlossenen RK angesehen werden mit $K_p = 0$
2. Teile, die auf der Reellen Achse zur WOK gehören herausfinden
   - Auf der Rellen Achse habe kompl. konj. Paare keinen Einfluss, da diese sich bzgl. Phaseneinfluss ausgleichen
   - Dadurch gehören die Punkte auf der reellen Achse zur WOK, die eine ungerade Anz. an PS/NS rechts neben sich haben => Pasenbedingung: $\phi = 180° + l\cdot 360°$
3. Pole für $K_P \to \infty$ bestimmen
   - Es werden n (Anz. PS) Pfade gesucht, auf denen die PS für $K_p \to\infty$ wandern. Die Asysmptoten gegen die $K_p$ läuft können berechenet werden über die Formel im nächsten Absatz
4. Austrittswinkel bestimmen
   - Für Austrittswinkel wird ein Testpunkt hergenommen, welcher in der sehr nahe des zu bestimmende Pols platziert wird.

### Phasenbedingung für Pfade $K_p \to\infty$

bzw. Asymptoten für $K_p\to\infty$

$$\phi_l^\infty = \frac{180°}{n-m} + l\frac{360°}{n-m}, \quad l = 0,...,n-m-1$$

mit $n$ = Anz. PS und $m$ = Anz. NS

## Kapitel 4: Frequenzantwort Design

### Systemtypen

 | System Typ | Folgt .. ohne Regelabw. |Steigung für $\omega \to 0$ | $K_{pos}$ | $e_{\infty ,step}$ | $K_{vel}$ | $e_{\infty ,ramp}$ |
 | :---: | :---: | :---: | :---: | :--: | :--: | :---: |
 | Typ 0 |  | konstanter Gain | $L(0)$ | $\frac{1}{1+K_{pos}}$ | $0$ | $\infty$ |
 | Typ 1 |  | Gain = -20dB/dek | $\infty$ | $0$ | $\frac{n_L(0)}{d_L(0)}$ | $\frac{1}{K_{vel}}$ |
 | Typ 2 |  | Gain = -40dB/dek | $\infty$ | $0$ | $\infty$ | $0$ |


## Wichtige, Diagramme und ihre Parameter (Unvollständig)

### Wurzelortskurve

- **Systemstabibiltät:** System ist auf jeden Fall stabil, wenn alle Pole und Nullstellen in LHE
- **Dämpfungsfaktor:** ist abzulesen als Winkel zwischen Reller Achse und dem Ortsvektor des dominierenden Pols
  - Am Besten 1; s. [Dämpfungsfaktor](###Dämpfungsfaktor)

### Bode Diagramm

#### Faustregeln fürs Zeichnen (Systeme 2. Ordnung; keine Pole in RHE)

- An Resonanzfrequenz beträgt Gain -3dB und $\varphi = -45°$
- Betragsgraph kann approximiert werden durch horizontale Gerade von $\omega = 0$ bis $\omega = \omega_n$ und ab diesem Punk als Gerade mit $-20dB/dek$
- Phasengraph kann approximiert werden durch $0°$ für $0\leq \omega \leq \frac{\omega_n}{5}$ und 90° für $5\cdot\omega_n \leq \omega \leq \infty$. Den Zwischenbereich kann man dann mit einer geraden approximieren.
- Handgezeichnetes Bodediagramm kann dann durch Ausgleichsgeraden gezeichnet werden, die alle Bedingungen erfüllen

#### Vorgehen bei Ordnung > 2

Zunächst sollte man die Übertragungsfunktion in die Bode Normalform bringen, da hier sowohl die Resonanzfrequenzen der einzelnen Pole als auch der static Gain direkt abgelesen werden können. Wenn man seine Übertragungsfunktion in der Form hat, dass sie ein Produkt aus Null- und Polstellentermen ist kann man durch ersetzen von $s$ durch $j\omega$ auf eine Form kommen, die von der Resonanzfrequenz abhängig ist. Danach muss nur noch jeder Null- bzw. Polstellenterm auf folgende Form gebracht werden: $(j \frac{\omega}{\omega_{ni}}+1)$.

Nach der Umformung ergibt sich eine Übertragungsfunktion folgender Form:

$$G(s) = K \frac{(j \frac{\omega}{\omega_{z1}}+1)...(j \frac{\omega}{\omega_{zm}}+1)}{(j \frac{\omega}{\omega_{p1}}+1)...(j \frac{\omega}{\omega_{pn}}+1)}$$

Die Resonanzfrequenzen können nun als $\omega_{zi}$ für die Nullstellen bzw. $\omega_{p_j}$ abgelesen werden. Der Static Gain ist in der Bode Normalform der Faktor $K$ vor dem Bruch.

:exclamation: Nicht jede Übertragungsfunktion lässt sich in der Bode Normalform darstellen. Wichtige Punkte die zu beachten sind:

- Alle Pole und NS müssen in der linken Halbebene oder im Ursprung liegen
- Alle Pole und NS müssen real sein
- Wenn es eine PS oder NS im Ursprung gibt ($s$ als Faktor) wird diese nur als $j\omega$ geschrieben

> **TODO** Anleitung zum Bode Plot zeichnen Skript S. 122

#### Instabile und Minimalphasige Systeme

- **Phase Margin:**
  - Abstand zu 180°(:question:) bei der Frequenz, wo der Verstärkungsgraph die 0dB Linie durchschreitet.
- **Gain Margin:**

**:exclamation: Wichtig:** Phase und Gain Margin können **NICHT** immer aus dem Bode Diagramm abgelesen werden. Für Systeme, die instabil bei kleinen Verstärkungen und stabil bei großen Verstärkungen sind sowie für nicht minimalpasige Systeme gelten die Regeln nicht! Ebenfalls ist zu beachten, dass die gegebene Definition von Gain. und Phase Margin nicht für Systeme anzuwenden ist, welche mehrere Durchtrittsfrequenzen der 0dB Linie haben. Hier muss dann das Nyquist Kriterium angewendet werden.

### Nyquist Diagramm

#### Zeichnen

Man verwende den Bode Plot als LUT für die Werte im Nyquist Diagramm. Für negative Frequenzen wird das Gezeichnete Diagramm einfach am Reeller Achse gespiegelt.

Falls das Nyquist Diagramm ins Unendliche strebt, kann man hier den Verlauf folgendermaßen nachvollziehen:

Da der Nyquist Pfad im Unendlichen schließt, wenn Pole oder NS im Ursprung liegen kann man bei dem Umlauf um die RHE einen infinetisemalen Kreis um den Ursprung legen. Auf diesem Kreis ordnet man nun von -90° über 0° bis 90° $i$ Testpunkte an mit $i=1+2 \cdot(\text{Anz. Pole/NS im Ursprung})$.

Nun muss die gesamten Phasenbeiträte aller Pole und NS an den Punkten aufsummiert werden. Dabei zählt der Winkel von Polen negativ und der von Nullstellen positiv. (:question:Es wird immer der Winkel zwischen Verbindungsvektor und einer gedachten parallelen Linie zur Reellen Achse gemessen.:question:)

#### Stabilitätstest

1. Zeichne Nyquist Diagramm von $KL(s)$
2. Zähnle Anz. $N$ an Umkreisungen im Uhrzeigersinn des Punktes $(-1, 0)^T$ und Anz. $P$ der instabilen Poles des Offenen RK (:question:Pole in RHE?)
3. => Geschlossner RK hat $Z=N+P$ instabile Pole

Wenn Offener RK Stabil dann ist Geschlossener RK Stabil, solange Nyquist Pfad nicht die -1 umschließt. Wenn $KL(S)$ einen instabilen Pol hat, dann ist Geschlossener RK Stabil, solange Nyquist Diagramm die $-1$ ein mal gegen den Uhrzeigersinn umkreist.

- **Phase Margin:**
  - Winkel zwischen -180° Phasor und Durchtrittspunkt durch Einheitskreis
  - Gibt Spielraum an, wie weit Phasendrehung möglich ist bevor -1 umkreist und somit System instabil wird. PM > 30° notwendig für akzeptables transientes Verhalten
- **Gain Margin:**
  - 1/Distandz zwischen Schnittpunkt Nyquist Diagramm mit Reeller Achse und 0
  - Gibt Verstärkung an ab der -1 Umschlossen wird und somit System instabil wird

#### System 2ter Ordnung Approximation

Für System 2ter Ordnung sind Peak Overshoot und Oszillation der Transienten Systemantwort vom Däpfungsfaktor $\zeta$ abhängig. Dieser kann über die Faustformel mit dem Phase Margin in Beziehung gesetzt werden:

$$\zeta \approx \frac{PM}{100}$$

Diese Approximation kann auch noch angewendet werden, wenn das dominierende Polpaar nahe an Imaginärer Achse ist.

## Wichtige Matlab Befehle

Für alle Befehle gilt:

- `G` ist eine Übertragungsfunktion

| Befehl | Argument | Erläuterung |
| :----: | :------: | :---------: |
| `G = tf([1 2 3],[3 2 1])` | $G(s) = \frac{s^2+2s+3}{3s^2+2s+1}$ | Argument sind die Polynomfaktoren als Vektoren. Die Übertragungsfunktion wird als `G` im Workspace abgelegt |
| `G = zpk([2 3],[4 5],6)` | $G(s) = 6\cdot\frac{(s-2)(s-3)}{(s-4)(s-5)}$ | Argument sind Nullstellen, und Polstellen als Vektoren `[]` und der Vorfaktor $K_p$ |
| `step(G)` | `G` | Plottet die Sprungantwort von `G` |
| `impulse(G)` | `G` | Plottet die Impulsantwort von `G` |
| `isstable(G)` | `G` | Gibt `True` (1) oder `False` (0) zurück, je nachdem ob System stabil oder nicht.

## Hilfreiche Klausurtipps nach Aufgabennummer

### Aufgabe 1

- Stabilität eines Systems bestimmen:
  - Alle Pol- und Nullstellen streng in LHE -> Stabil, ggf. ausklammern
  - Nyquist Kriterium
  - Grenzstabil, wenn midestens ein Pol auf Imaginärer Achse. Integratoren (PS im Ursprung) zählen auch.
- Ordnung eines Systems
  - Grad des höchsten Nenner- bzw. Zählerpolynoms
- Typ eines Systems: Anz. an Integratoren (PS im Ursprung) bzw. bei Ausklammern $\frac{1}{s^n}$ Nummer $n$
- Skizzieren Sprungantwort
  - Integratoren vorhanden -> konstant ansteigend
  - Komplex konjugiertes Polpaar -> Schwingung
    - Abklingend bei Stabilem System (Polpaar in LHE), Dauerschwingung bei Grenzstabilem System (Im Achse) und Ansteigend bei instabilem System (RHE)
- Typische Matlab befehle siehe [Matlab Befehle](##WichtigeMatlabBefehle)

### Aufgabe 2

## TODO

- Bode Plot
  - [ ] Zeichnungsschritte einfügen (S. 122)
  - [ ] Verhalten von komplex konjugierten Polpaaren (ab S. 123)