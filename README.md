# ET Module

Hinweise und Formelsammlungen von meinem ET Studium an der TUHH. Nutzung für eigene Zwecke erlaubt, Verbreitung ohne Quelle verboten.

## Markdown Dateien

Anzuschauen sind die Markdown Dateien (.md) am besten mit einem lokalen Editor, der die Latex Syntax unterstützt. Ich verwende zum editieren Visual Studio Code mit [der Mardown All in](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) für Latex-Mathe und einer Erweiterung, welche Emojis rendern kann. Das sind die Ausdrücke zwischen zwei Doppelpunkten. Ich habe dafür [diese Erweiterung](https://marketplace.visualstudio.com/items?itemName=bierner.github-markdown-preview) installiert.

## LaTeX Dateien

Ich versuche möglichst immer auch eine aktuelle PDF Datei mit zu comitten, wenn eine LaTeX Datei Committed wird. Falls du aber gerne selber Änderungen vornehmen möchtest und keine Erfahrungen mit LaTeX hast dann hier eine kleine Installationsanleitung:

### Windows

- [MikTeX](https://miktex.org/download) downloaden und installieren
- [Perl](https://strawberryperl.com/) herunterladen und installieren, sofern nicht vorhanden auf deinem PC
- System Neustart durchführen


### Editor installieren/einrichten (Windows und Linux probiert. Mac habe ich nicht)

Ich kann folgende Editoren empfehlen:

1. [TeXmaker](https://www.xm1math.net/texmaker/)
2. Visual Studio Code mit der Erweiterung [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)

## Allgemeines

Jeder, der Korrekturen oder Erweiterungen vornimmt ist eingeladen einen Pull Request zu starten.
