# Wireless Systems for Mobile Communications - Themen/Inhaltsverzeichnis

## 1 - Einleitung

Unwichtiger Kram wer wer ist....

## 2 - Radio channel characteristics

- Properties of the propagation Channel :heavy_check_mark:
  - Transmission Channel Aufbau (Was ist der Propagation channel?)
  - Objektbezogene Interferernzen EM Wellen auf Signalweg
    - Abschattung, Reflektion, Brechung, Streuung und Beugung
  - Weritere Physikalische Phänomene
    - Ausbreitungsverzögerung, Freiraumdämpfung, Mehrwegeausbreitung, Dopplerverschiebung
- Description in frequency domain :heavy_check_mark:
  - Schmalbandiges Modell bei einer Frequenz ausgewertet (Dirac im Frequenzbereich)
  - Frage: Wie sieht Empfangssignal in Bezug auf **Amplitude oder Leistung**, **Empfangsfrequenz** bzw. **Empfangsphase** abhängig von Position und Geschwindgkeit von Empfänger aus?
  - Dämpfung auf Signalweg $L_{total}$ = Freiraumdämpfung $L_{PL}$ + Abschattung (slow fading) $L_{SF}$ + Schnelles fading (fast fading) $L_{FF}$
  - Abstandsabhängige Dämpfung durch Verteilung von Sendeleistung auf Kugeloberfläche. Empfangsleistung abhängig von Leistungsdiche an Empfänger und effektiven Antennenfläche $A_{eff}$: $P_{rx}=A_{rx}S=A_{rx}\frac{P_{tx}}{4\pi d^2} $
  - Bei realen Antennen hängt $P_{rx}$ auch von Gewinn einer Antenne in Empfängerrichtung ab: $P_{rx} = P_{tx}G_{tx}\frac{1}{4\pi r^2}A_{rx} $
  - Mit $A_{rx}(\theta, \phi) = \frac{\lambda^2}{4\pi}G_{rx}(\theta, \phi) $ ergibt sich Friis'sche transmissionsgleichung:
$$
    P_{rx} = P_{tx}G_{tx}G_{rx}(\frac{\lambda}{4\pi d})^2
$$
  - Nur gültig im Fernfeld, also $d>\frac{2L^2}{\lambda} $
  - Erhöhung der Felddämpfung mit höherer Frequenz liegt an verringerung der effektiven Antennenfläche
  - Frequenzverschiebung durch Doppler Effekt:
$$
    f_{rx} = f_{tx} + f_d = f_{tx} + \frac{v}{c_0}f_{tx}\,cos\,\alpha_n
$$
  - Dopplerverschiebung nur von Geschwindigkeitsanteil auf direkter Verbindungslinie zwischen Sender und Empfänger. Kann über Winkel $\alpha_n$ ausgedrückt werden
    - -> Resultierende Dopplerverschiebung bei gegebener Relativgeschwindigkeit zwischen Ziel und Radarsystem kann also zwischen $f_{d,min}=0\frac{m}{s}$ und $f_{d,max} = \frac{v}{c_0}f_{tx}$ liegen
  - Bei Mehrwegeausbreitung hat jeder Pfad eigene Relativgeschwindigkeit und Winkel -> unterscheidliche Frequenzverschiebungen -> Verbreiterung des Empfangsspektrums (**Frequenzdispersion**)
    - Frequenzdispersion signifikantestes Probelem bei Dopplerefefkt, da einfache Verschiebung leicht korrigiert werden könnte
  - Kosequenzen
    - Fast Fading -> Durch Beat Frequencys bei 2 Trägern nahe beieinander
    - Mögliche Inter Carrier Interferenz (ICI) vor allem bei OFDM
    - Zeitselektrives Verhalten des Kanals
- Description  in time domain
- Connection time and frequency domain
- Conclusion & Outlook

## 3 - Radio channel modelling :heavy_check_mark:

- Transmission Channel
  - Kanal verzerrt/stört
  - Grundsätzlich 2 mgöichkeiten der Störung: Multiplikatives und additives Rauschen
    - Multiplikativ: Path loss, Abschattung, Fast Fading
    - Additiv: Rauschen von anderen Störern, Dopplereffekt (vor allem bei Mehrwegeausbreitung)
  - Modellierung in bis zu 5 Dimensionen: Zeit, Frequenz und bis zu 3 Raumkoordinaten
- Path-loss models
  - nichts markiert
- Non frequency selective fading
  - **Rayleigh fading** Modell: Stochastisches Modell zur Modellierung von **Fast Fading** effekten bei **Mehrwegeausbreitung** über N statisch unabhängige Pfade **ohne LOS/Dominanten** Pfad
  - **Rice fading** ist Erweiterung des Rayleigh Modells um LOS Pfad   
    - Anwendbar z.B. bei: Richtfunk, Mobile Anwendungen mit Sichtverbindung zu Basisstation, Kleine Zellradien, kleiner Distanz zw. Tx und Rx
- Frequency selective fading
  - Wichtige Kanalparameter:
    - Zeitbereichs Koohärenzzeit: Zeit, in der Kanal als gleichbleibend angenommen werden kann. Sich nicht verändernder Kanal hat $T_{coh}=\infty$ und Dopperspread $B_{DS}=0$
    - Frequenzbereichs Kohärenzbandbreite: Fourier transformierte des PDP, Bandbreite ind der Frequenzantwort nahezu konstant. Keine Zeitdispersion: $T_{ds}=0$ heißt $B_{coh}=\infty$
  - S. 86 fehlt noch

## 4 - Modulation techniques

- Signal theory
- Modulation tehcniques
- Amplitude Modulation
- Angular Modulation
- Transmitter architectures
  - Direct dual sideband transmitter
  - Direct frequency modulation
  - IQ Transmitter
  - Imtermediate Frequency/Digital IF Transmitter
- Receiver architectures
  - Detector Receiver
  - Direct conversion receiver
  - Superheterodyne receiver
  - Direct conversion IQ Receiver
- Transceiver architectures
  - 
- Transceiver architecture

## 5 - Digital modulation

- Phase shift keying
  - M-PSK (BPSK, QPSK, 8-PSK, ..., M-PSK)
  - DPSK
  - OQPSK
  - MSK
  - GMSK
- Quadrature Amplitude Modulation
  - QAM ist PSK mit zusätzlicher Auswertung der Signalamplitude
- Multiplexing
  - Time Division Multiplex (TDM)
  - Space Division Multiplex (SDM)
  - Frequency Division Multiplex (FDM)
  - Orthogonal Frequency-Division Multiplexing (OFDM)
  - Code Dision Multiplex (CDM)

## 6 - Localization in wireless systems

- Localization in wireless Systems
  - Positioning: Ausschließlich Bestimmung der Position einer Person oder Objektes
  - Tracking: Überwachung der Bewegung einer Person oder eines Objektes
  - Navigation: Routenführung und Unterstützung von Start zum Ziel
  - Komplexität des Positionierungssystems abhängig von: Akkuratheit und Präzision der geschätzten Position; Time to first fix; Power Budget; Verfügbare Infrastruktur
  - Grundlegende Prinzipien:
    - Raumakkurat: Vorhandensein eines Referenzsignals
    - Fingerprinting: Mapping auf bekannte Signalcharakteristika
    - Orientation: Einfallswinkel von Referenzsignal
    - Grobe relative Distanz: Relative Leistung von Referenzsignal
    - Präzise relative Distanz: Ausbreitungszeit/Laufzeit von Referenzsignal
- Review on positioning systems
  - Ground Based: DECCA, LORAN, OMEGA
  - Satellitenbasiert: Globla Navigation Satellite System (GNSS)
    - Meist Satelliten im Medium Earth Orbit (MEO), also ca. 20 000 km über der Erdoberfläche
    - Grundlegendes Prinzip: Zeitdifferenz zwischen von verschiedenen Satelliten empfangene Absolute Uhrzeit und bekannte Satellitenpositionen
    - Gängigste Systeme: GPS, GLONASS, Galileo
- Positioning principles
- Angle of arrival
- Fingerprinting
- Localization in wireless sensor networks

## 7 - Radar for mobile applications

- Radar equation
- Continous wave radar
- Frequency modulated continous wave radar
- Radar signalprocessing basics
  
## 8 - Ultra-low power communications