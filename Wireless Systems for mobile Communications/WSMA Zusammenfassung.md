# WSMA Zusammenfassung

## 1 - Einleitung

In vielen Geräten sind Funkchips eingebaut, welche entweder zur Kommunikation mit einer Basisstation oder direkt zwischen den eingesetzten Geräten verwendet werden. Dabei sind vor allem die mobilen Anwendungen besonders herausfordernd, da hier schnelle Änderungen im Ausbreitungskanal die Regel sind.

## 2 - Funkkanal Eigenschaften und Charakterisierung

Ein **Übertragungskanal** kann in verschiedene Teilbereiche zerlegt werden:
- Kleinster Teil: Ausbreitungskanal. Beinhaltet nur die Ausbreitung der EM Wellen
- Drahtloser Kanal: Zusätzlich Antennen
- Modulationskanal: Zustätzlich RF Frontend + ggf. Modulator
- Digitaler Kanal: Zusätzlich Kanalkodierer

Zunächst sollen die Effekte im Ausbreitungskanal, also nur der Weg zwischen den Rx/Tx Antennen, betrachtet werden.

In realen Ausbreitungsszenarien wird die Empfangsantenne immer ein Signal über verschiedene Wege erhalten, welche am Empfangsort miteinander interferieren bzw. sich überlagern. Dabei ist meist der dominante Pfad derjenige, welcher die direkte Sichtverbindung zwischen Sende- und Empfangsantenne darstellt, auch der Line-of-sight Pfad oder kur **LOS** Pfad genannt. Alle anderen Pfade, welche durch interaktion mit Objekten in der Umbegung zum Emfangssignal beitragen werden auch Non-Line-Of-Sight oder **NLOS** Pfad genannt.

Auf allen Pfaden sorgt die Freiraumdämmpfung dafür, dass mit zunehmender Pfadlänge die Signalstärke quadratisch mit der Entfernung abnimmt. Diese Freiraumdämpfung ist keine Dämpfung im eigentlichen Sinne, bei der EM-Energie in Wärme umgewandelt wird, sondern beschreibt den Effekt, dass sich die ausgesendete Energie $P_{tx}$ auf einer Kugeloberfläche verteilt. Die empfangene Leistung hängt nun davon ab, wie groß die Fläche ist, auf der die Energie aufgenommen werden kann. Die Freiraumdämpfung kann somit als Verhältnis zwischen der Empfangsfläche und der Kugeloberfläche in einem gegebenen Abstand $r$ beschrieben werden: 
$$\frac{A_{Ant}}{A_{Kugel}} = \frac{A_{Ant}}{2\pi r^2}$$

Darüber hinaus kann natürlich auf allen Pfaden auch noch das Medium zwischen Sender und Empfänger dämpfend wirken, wenn z.B. bei Regen sehr viel Wasser in der Luft ist oder die EM-Wellen durch verlustbehaftete Materialien auf ihrem Weg laufen.

Alle Objektbezogenen Interaktionen von EM-Wellen:
- Abschattung
- Reflektion
- Beugung
- Brechung
- Streuunga

Weitere Physikalische Phänomene der Ausbreitungspfades:
- Ausbreitungsverzögerung
- Hochfrequenzfeld Dämpfung
- Mehrwegeausbreitung
- Doppler Effekt

### Beschreibung des Ausbreitungskanals

Um alle Phänomene im Ausbreitungskanal einfach beschreiben zu können, ist es Sinnvoll einige Effekte schmalbandig im Frequenzbereich und andere breitbandig anhand von z.B. Dirac Impulsen im Zeitbereich zu analysieren.

Für eine **Analyse im Frequenzbereich** bieten sich alle Effekte an, welche direkt auf die Frequenz, Phase und Frequenzabhängige Amplitude Einfluss haben. Hierzu zählen z.B. der Doppler Effekt und eine Frequenzselektivität des Kanals.

Auf der anderen Seite kann die **Analyse im Zeitbereich** helfen Effekte der Ausbreitungsverzögerung und hier vor allem die Effekte der Mehrwegeausbreitung zu verstehen.

## 5 - Digitale Modulation

Die grundlegenden Modulationsprinzipien der digitalen Modulation sind identisch zu denen der analogen Modulation. Der Unterschied besteht allerdings darin, dass bei der digitalen Modulation nur diskrete Werte für Amplitude, Frequenz und Phase zugelassen werden, welche dann die einzelnen Symbole darstellen. Dabei kann pro Symbol entweder ein Bit, auch binäre Modulation genannt, oder mehrere Bits codiert werden. Besondere Herausforderungen sind der bandbreitensparende und Verstärkerschonende Übergang zwischen den einzelnen Symbolen.

In diesem Kapitel wichtige Punkte:
- Unterschied ASK, FSK und PSK
- Bedeutung hoher Amplitudenfluktuation -> PAPR
- Besondere Modulationsverfahren:
  - DPSK -> Phasendrehung codiert Datensymbole, nicht absolute Phase
  - OQPSK -> QPSK, bei der I und Q Komponente nicht gleichzeitig verändert werden, sodass Amplitudenfluktuation minimiert wird
  - MSK -> FSK, bei der die Phase beim Sprung zwischen Frequenzen erhalten bleibt und nicht springt
  - GMSK -> MSK mit Gaußfilter über Phase. Geringere Bandbreite, dafür können Symbole "verwaschen"
  - QAM -> Nutzung von Phase und Amplitude zu codierung von Datenwörtern/Symbolen
- Multiplexingverfahren (Auch in Kombination verwendet):
  - Time Division Multiplexing (TDM)
  - Space Division Multiplexing (SDM)
    - Mehrfache Benutzung des gleichen Kanals in weit genug voneinander entfernten Breichen (Zellen)
    - Ansprechen nur einer Richtung durch Direktivität -> Beaamforming, MIMO
  - Frequency devision Multiplexing (FDM)
    - Aufspaltung eines Spektrums in viele schmalbandigere Kanäle -> Symboldauer pro Träger erhöht sich, sodass "Nachhall" durch Mehrwegeausbreitung im Kanal weniger schlimm ist
  - Orthogonal FDM (OFDM) -> Wie FDM, nur sind Träger Orthogonal zueinander ausgerichtet, d.h. Träger sind nur bei Frequenzen bei denen das Spektrum aller anderen Subträger eine Nullstelle aufweist. Spacing: $\Delta f = \frac{1}{T_{Symbol}}$
  - Generalized FDM (GFDM)
  - Code Division Multiplex (CDM) -> Nutzung von Spreizsequenzen zur Unterbringung mehrerer Sendesignale auf einer Trägerfrequenz. Dadurch deutlich höherer Bandbreitenbedarf als minimal nötig, aber höhere Immunität gegen schmalbandige Störer.

## 6 - Lokalisation in drahtlosen Systemen (Ab S. 191)

Um drahtlos die Position eines Gerätes zu bestimmen kann die Komplexität an verschiedenen Stellen im System platziert werden. Das wohl bekannsteste System ist hier das **Global Navigation Satellite System** (**GNSS**), welches die gängigen Systeme wie GPS, GLONASS, Galileo usw. unter sich vereint. Hier ist jeder Satellit im Grunde nur eine hochpräzise Uhr mit einem Sender, welche dessen Zeit drahtlos ausstrahlt. Im Endgerät wird dann aus den Zeitverschiebungen und den bekannten Satellitenpositionen die relativ genaue eine Positionsschätzung bestimmt. Bei diesem System spricht man von der Selbstlokalisierung, da die gesamte Rechenleistung im Endgerät enthalten ist. 

**FRAGE**: Ab wann hat man ein unterstütztes System? Reicht Almanachübertragung schon für die Anforderung? Oder ist untertstützt erst, wenn man Daten Aktiv zu einer Berechnungseinheit sendet?

**Eigener Definitionsansatz:** Selbstlokalisierung ist, sofern lokalisierendes Gerät nur empfängt und Berechnung komplett alleine macht. Assisteirte Lokalisierung ist wenn das lokalisierende Gerät z.B. Laufzeitunterschiede empfängt und diese Daten zur Positionsbestimmung an eine zentrale Recheneinheit sendet. Netzwerkbasierte Lokalisierung wird von (vermutlich mehreren) Basisstationen durchgeführt auf Grundlage der von einem Mobilgerät empfangenen Signale durchgeführt.

- Lokalisierungsmethoden: Netzwerkbasiert, Assistiert oder Selbstlokalisiserung
- Typische Szenarien: Indoor oder Outdoor
- Problematisch: Power Budget
- Definitionen:
  - Positionierung: Bestimmung von nur der Position
  - Tracking: Überwachung der Beweugung einer Person oder eines Objektes
  - Navigation: Wegbestimmung und Leiten von Start- zu Zielposition
- Drahtlose Positionierung hat folgende Tradeoffs: Akkuratheit und Präzision der geschätzten Position, Zeit bis zum ersten Fix (Time to first Fix), Power Budget, Verfügbare Infrastruktur
- Grundlegende Prinzipien der drahtlosen Positionsbestimmung
  - Präsenz von Referenzsignal ("Raumakkurat")
  - Mapping zu bekannten Signalcharakteristika ("Fingerprinting")
  - Einfallswinkel von Referenzsignal (Orientation)
  - Relative Signalstärke von Referenzsignal RSSI (Grobe relative Distanz)
  - Ausbreitungszeit von Referenzsignal (Präzise relative Distanz)
  - 

## 7 - Radar for mobile communications (Ab S. 234)

RADAR ist ein Akronym, welches für RAdio Detection And Ranging steht. Kurz gesagt geht es also bei der Thematik um die Erkennung von Objekten und Abstandsmessung dieser zu einer Antenne mit Radiowellen. Neben den beiden Informationen ist es ebenfalls noch möglich die Radiale Geschwindigkeit, also wie schnell ein Objekt sich von einem Radar System weg- oder hinbewegt, nicht aber die Absolutgeschwindigkeit, und der Winkel in Bezug auf das Radarsystem ermittelt werden.

Das grundlegende Messprinzip beruht darauf, dass sich elektromagnetische Wellen mit einer konstanten Geschwindigkeit, nämlich nahezu der Lichtgeschwindigkeit, im freien Raum ausbreiten. Wenn man nun also die Zeit zwischen Aussendung eines Radiosignals und des Empfangs von dessen Reflektion an einem Objekt misst kann darüber im einfachsten Fall die Distanz berechnet werden.

Für eine besonders hohe örtliche Auflösung ist zum einen eine hohe Abtastrate notwendig und auf der anderen Seite muss der Radiopuls möglichst kurz sein. Dies sind auch schon die größten Probleme bei der realen Implementierung eines solchen Radarsystems. Aus der Lichgeschwindigkeit ergibt sich, dass Radiowellen sich 30cm pro Nanosekunde ausbreiten. Daraus kommt man bei einer Abtastrate von 1 MHz bestenfalls auf eine Auflösung von 30cm. Für 3cm ist schon eine Abtastrate von 1GHz notwendig. Das Gleiche Problem besteht auch mit dem eigentlichen Radarpuls. Dieser muss vollständig abgeklungen sein bevor das erste Echo auswertbar ist. Soll also in einem Meter Abstand zum Radargerät eine Messung mit dem einfachen Zeitmessungs Ansatz durchgeführt werden muss der Puls kürzer als 3 ns sein. Hieraus ergben sich hohe Anforderungen an den Signalgenerator und die benötigte Bandbreite. Zusammengefasst kann man also sagen, dass dieser einfache Ansatz zwar simpel aber auch sehr begrenzt einsetzbar ist.

In der Radartechnik gibt es zum einen das Primärradar, welches auch als nicht kooperatives Radar bezeichnet werden kann. Hierbei wird ein Radarpuls ausgesendet und alleine das Echo von Objekten, wie z.B. Schiffen, Flugzeugen, Regenwolken usw., ausgewertet. Beim Sekundärradar sind die Radarziele ebenfalls mit aktiver Sende- und Empfangstechnik ausgestattet. Dies hat gegenüber dem Primärradar den Vorteil, dass deutlich höhere Reichweiten umsetzbar sind, was sich in der Freiraumdämpfung begründet. Bei einem reinen Primärradar gibt es zweimal auf der Strecke zwischen Sender und Radarziel Verluste durch die Freiraumdämpfung. Da diese zweimal als Faktor $r^2$ in die Gleichung eingeht wird das Sendesignal alleine durch den Weg im Freiraum mit dem Faktor $r^4$ gedämpft. Wenn man nun das Sekundärradar betrachtet ist hier nur der einfache Weg entscheidend, da das Radarziel nach Empfang des Radarpulses der Basisstation sein eigenes erneutes Signal aussendet. Der Nachteil vom Sekundärradar ist allerdings, dass nicht mit Technik ausgestattete Objekte nicht erkannt werden.

In der Luftfahrt ist ein System aus Primär und Sekundärradaren eingesetzt, um auf der einen Seite alle Objekte erkennen zu können und auf der anderen Seite eine sehr Hohe Präzision zu erreichen. Hierbei wird sich zu Nutze gemacht, dass in der Antwort des Radarziels auch Daten übertragen werden können. So wir bei dem gängigen System ADS-B neben den GPS Koordinaten auch noch weitere wichtige Informationen, wie z.B. Höhe und Geschwindigkeit übertragen. Diese Nachrichten ermöglichen es somit jedem Flugzeug den Verkehr in der näheren Umgebung digital zu sehen und automatisch Ausweichmanöver einzulieten, sollten sich zwei Flugzeuge auf Kollisionskurs befinden.

- 2 Mögliche Antennenkonfigurationen:
  - Monostatisch: Eine gemeinsame Sende- und Empfangsantenne
  - Bistatisch: Je eine Sende und eine Empfangsantenne
- Radargleichung:
$$
S_t = \frac{P_tG_t}{4\pi R^2}
$$
  - $S_t$: Leistungsdichte am Ziel
  - $P_t$: Sendeleistung
  - $G_t$: Gain der Sendeantenne
  - $R$: Distanz zwischen Sender und reflektierenden Objekt
- Ableitung der Radargleichung: Radar cross section. Verhälnis aus Leistungsdichte am Ziel und zurückgestrahlter Leistung
$$
\sigma = \frac{P_s}{S_t} \,\,\,\,\,\,[m^2]
$$
- $\sigma$ abhängig von Material, Geometrie, Winkel und Polarisation
- Gesamte Radargeleichung:
$$
P_r = \frac{P_tG_t\sigma G_r\lambda^2}{(4\pi R^2)^2(4\pi)}
$$

### Signal im Radar System

1. Aussendung eines Pulses mit Sendeleistung $P_t$ und Antennengewinn $G_t$
2. Leistungsdichte am Ziel im Abstand von R nach Freiraumdämpfung: $S_t = \frac{P_tG_t}{4\pi R^2}$
3. Von Ziel mit Radarquerschnitt $\sigma$ reflektierte Leistung: $P_s=S_t\sigma = \frac{P_tG_t\sigma}{4\pi R^2}$
4. An Antenne vorliegende Leistungsdichte $S_r$ nach Reflektion: $S_r=\frac{P_s}{4\pi R^2}$
5. Empfangene Leistung (Leistungsdichte mal effektive Antennenfläche): 
$$
P_r = S_r\cdot A_{eff} = \frac{P_s}{4\pi R^2}\cdot \frac{G_r\lambda^2}{4\pi} = \frac{P_tG_t\sigma}{(4\pi R^2)^2}\cdot \frac{G_r\lambda^2}{4\pi} = \frac{P_tG_tG_r\sigma\lambda ^2}{(4\pi R^2)^2\cdot (4\pi)}
$$

### Die Radargleichung

$$
P_r = \frac{P_tG_t\sigma G_r\lambda ^2}{(4\pi)^3 R^4}
$$
- Sendeleitung $P_t$
- ...

.
.
.

## 8 - Ultra-low Power Communications (Ab S. 287)

- Keyless Entry/Keyless Go/Remote Keyless Entry: Zugang zu Objekten und Funktionen ohne mechnaische Interaktion unter Benutzung von Radiowellen
  - Vorwiegend in Automobilbereich angesiedelt
- LF Kanal Eigenschaften (Keyless Entry)
  - 125kHz/130kHz (LF) oder 20kHz(VLF)
  - Reichweite: 2m angestrebt mit induktiver Kopplung
  - Aufgrund von Wellenlänge im km Bereich sehr schwache magnetische Kopplung zw. Sende- und Empfangsspule
  - Reichweite proportional zur 6ten Wurzel der Sendeleistung
  - Magnetfeld nimmt mit 1/r³ ab
  - Gute Durchdringung von Materie (z.B. Körper direkt neben Schlüssel)
- UHF Kanal Eigenschaften
  - ISM Knaäle: EU 433/868MHz, JP/US 315 MHz
  - Reichweite mehrere 10m

### Keyless Go Kanäle
| Eigenschaft | LF Kanal | UHF Kanal |
| :-: | :-: | :-: | :-: |
| Frequenz | 125/130/20kHz | EU 433/868 MHz; JP/US 315 MHz |
| Reichweite | 2m | mehrere 10m |
| Reichweite soll Keyless Go | 10cm | 10cm |
| Kopplung | Magnetisch über Spulen | Elektromagnetisch über Loop oder Dipol |
| Reichweite | Proportional zur 6ten Wurzel der Sendeleistung | Proportional zu 2ten Wurzel der Sendeleistung |
| Feldverhalten | Magn. Feld fällt mit $\frac{1}{r^3}$ ab | Empfangene Leistung fällt mit $\frac{1}{r^2}$ ab |
| Durchdringung | relativ gut | Gering, da Abschattung durch Körper signifikant |

## Keyless Go System Struktur

- Auto hat 125kHz Tx und 433 MHz Rx
- Schlüssel hat 125 kHz 3D-Empfangsspule mit Wakeup Receiver zum Aufwachen von uC
  - Nach Aufwecken von uC über VLF/LF sendet Schlüssel auf UHF Authetifizierungsdaten
- Nach erfolgreicher Authentifizierung geht Schlüssel wieder schlafen

### Sicherheitslücken gängier KE Systeme

- Schwache Kryptographie
  - Schlüssel können kopiert werden
  - **Gegenmaßnahme**: Kryptographie verstärken -> Umgesetzt
- Jamming (LF/UHF)
  - Funktion des Schlüssels kann verhindert werden bei blockierung der Kommunikationsfrequenz
  - **Gegenmaßnahme**: 3-Frequenz-System, Multiband Operation
- Replay Attacken
  - Aufnahme und Wiedergabe der UHF Kommunikation
  - **Gegenmaßnahme**: Starke Codierung
- Relay Station Atacke
  - Kommunikation zwischen Schlüssel und Auto über Relaistelle verlängern ohne decodierung der Nachrichten
  - **Problem**: Bei den meisten Autos heutzutage noch reales Sicherheitsrisiko
  - **Gegenmaßnahme**: Triangulation, Feldstärkemessung, ToF Messung
    - Messung der Signallaufzeit
    - Bestimmung vom IP3 durch Mischprodukte
    - Messung elektrischer Feldstärke und erkennung übermäßiger Feldstärke Werte
    - Bestimmung der Abklingzeit von LC Resonanzkreis bei LF
    - 